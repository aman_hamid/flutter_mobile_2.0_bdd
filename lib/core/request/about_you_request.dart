import 'package:pocketpills/core/request/base_request.dart';

class AboutYouRequest extends BaseRequest {
  String? province;
  bool? hasDailyMedication;
  bool? isCaregiver;
  bool? pocketPacks;
  String? email;
  String? gender;
  String? invitationCode;
  String? referralCode;
  String? password;
  String? phone;
  String? latestPrescriptionDeliveryBy;

  AboutYouRequest(
      {this.password, this.invitationCode, this.province, this.hasDailyMedication, this.isCaregiver, this.pocketPacks,
        this.email, this.gender, this.referralCode, this.phone,this.latestPrescriptionDeliveryBy});

  Map<String, dynamic> toJson() => <String, dynamic>{
        'province': this.province,
        'pocketPacks': this.pocketPacks,
        'hasDailyMedication': this.hasDailyMedication.toString(),
        'isCaregiver': this.isCaregiver.toString(),
        'email': this.email,
        'gender': this.gender,
        'invitationCode': this.invitationCode,
        'referralCode': this.referralCode,
        'password': this.password,
        'phone': this.phone,
        'latestPrescriptionDeliveryBy': this.latestPrescriptionDeliveryBy,
      };
}
