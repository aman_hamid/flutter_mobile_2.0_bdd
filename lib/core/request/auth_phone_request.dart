import 'package:pocketpills/core/request/base_request.dart';

class AuthPhoneRequest extends BaseRequest {
  String? userIdentifier;
  String? phone;
  String? userFlow;

  AuthPhoneRequest({this.userIdentifier, this.phone, this.userFlow});

  Map<String, dynamic> toJson() => <String, dynamic>{'userIdentifier': this.userIdentifier, 'phone': this.phone, 'userFlow': this.userFlow};
}
