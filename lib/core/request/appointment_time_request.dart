import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/request/base_request.dart';
import 'package:pocketpills/core/utils/date_utils.dart';

class AppointmentTimeRequest extends BaseRequest {
  @JsonKey(fromJson: PPDateUtils.fromStr, toJson: PPDateUtils.toStr)
  DateTime? appointmentTime;

  AppointmentTimeRequest({this.appointmentTime});

  Map<String, dynamic> toJson() => <String, dynamic>{
        'appointmentTime': PPDateUtils.toStr(this.appointmentTime),
      };
}
