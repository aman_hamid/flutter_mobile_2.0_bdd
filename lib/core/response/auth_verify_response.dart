import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/auth_verification.dart';
import 'package:pocketpills/core/models/phone_verification.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

part 'auth_verify_response.g.dart';

@JsonSerializable()
class AuthVerifyResponse {
  @JsonKey(name: 'success')
  bool? status;

  @JsonKey(name: 'userMessage')
  String? errMessage;

  @JsonKey(name: 'message')
  String? apiMessage;

  @JsonKey(name: 'details')
  AuthVerification? response;

  AuthVerifyResponse({this.status, this.errMessage, this.apiMessage, this.response});

  String getErrorMessage() {
    if (errMessage != null && errMessage != "")
      return errMessage!;
    else
      return LocalizationUtils.getSingleValueString("common", "common.label.api-error");
  }

  factory AuthVerifyResponse.fromJson(Map<String, dynamic> json) => _$AuthVerifyResponseFromJson(json);
}
