import 'package:json_annotation/json_annotation.dart';

part 'free_vitamins_response.g.dart';

@JsonSerializable()
class FreeVitaminsResponse {
  bool valid;

  FreeVitaminsResponse({required this.valid});

  factory FreeVitaminsResponse.fromJson(Map<String, dynamic> json) => _$FreeVitaminsResponseFromJson(json);

  Map<String, dynamic> toJson() => _$FreeVitaminsResponseToJson(this);
}
