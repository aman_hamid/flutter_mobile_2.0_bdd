import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/patient.dart';
part 'signupEDDitem.g.dart';

@JsonSerializable()
class SignupEddItem {
  @JsonKey(name: 'value')
  String value;

  @JsonKey(name: 'label')
  String label;

  SignupEddItem(
    this.value,
    this.label,
  );

  factory SignupEddItem.fromJson(Map<String, dynamic> json) => _$SignupEddItemFromJson(json);
}
