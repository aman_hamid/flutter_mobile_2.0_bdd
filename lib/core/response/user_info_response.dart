
import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/signup_dto.dart';
import 'package:pocketpills/core/models/transfer_prescription.dart';
part 'user_info_response.g.dart';

@JsonSerializable()
class UserInfoResponse extends Object{

  @JsonKey(name: 'userId')
  int userId;

  @JsonKey(name: 'prescription')
  TransferPrescription prescription;

  @JsonKey(name: 'signupDto')
  SignupDto signupDto;


  UserInfoResponse(this.userId, this.prescription, this.signupDto);

  factory UserInfoResponse.fromJson(Map<String, dynamic> json) =>
      _$UserInfoResponseFromJson(json);
}
