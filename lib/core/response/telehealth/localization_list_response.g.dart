// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'localization_list_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LocalizationListResponse _$LocalizationListResponseFromJson(
    Map<String, dynamic> json) {
  return LocalizationListResponse(
    (json['data'] as List<dynamic>?)
        ?.map((e) =>
            LocalizationUpdateResponse.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$LocalizationListResponseToJson(
        LocalizationListResponse instance) =>
    <String, dynamic>{
      'data': instance.dataList,
    };
