import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/patient.dart';
part 'telehealth_province.g.dart';

@JsonSerializable()
class TelehealthProvinceItem {
  @JsonKey(name: 'value')
  String value;

  @JsonKey(name: 'label')
  String label;

  TelehealthProvinceItem(
    this.value,
    this.label,
  );

  factory TelehealthProvinceItem.fromJson(Map<String, dynamic> json) => _$TelehealthProvinceItemFromJson(json);
}
