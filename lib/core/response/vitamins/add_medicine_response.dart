import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/response/vitamins/coupon_code.dart';
import 'package:pocketpills/core/response/vitamins/shopping_cart_item.dart';
part 'add_medicine_response.g.dart';

@JsonSerializable()
class AddMedicineResponse {
  int? id;
  bool? disabled;
  int? patientId;
  CouponCode? couponCode;
  int? addressId;
  int? ccId;
  num? listPrice;
  num? totalPrice;

  List<ShoppingCartItem>? shoppingCartItems;

  AddMedicineResponse(this.id, this.disabled, this.patientId, this.couponCode, this.addressId, this.ccId, this.listPrice, this.totalPrice, this.shoppingCartItems);

  factory AddMedicineResponse.fromJson(Map<String, dynamic> json) => _$AddMedicineResponseFromJson(json);
}
