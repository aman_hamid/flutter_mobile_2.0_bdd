import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/response/pillreminder/pocket_pack_details.dart';
part 'pill_reminder_medications_response.g.dart';

@JsonSerializable()
class PillReminderMedicationResponse {
  List<PocketPackDetails> pocketPackDetails;

  PillReminderMedicationResponse(this.pocketPackDetails);

  factory PillReminderMedicationResponse.fromJson(Map<String, dynamic> json) =>
      _$PillReminderMedicationResponseFromJson(json);
}
