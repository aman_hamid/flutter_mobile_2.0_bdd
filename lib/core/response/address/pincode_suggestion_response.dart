import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/response/address/pincode_suggestion.dart';

part 'pincode_suggestion_response.g.dart';

@JsonSerializable()
class PinCodeSuggestionResponse {
  @JsonKey(name: 'items')
  List<PinCodeSuggestion> items;

  PinCodeSuggestionResponse(this.items);

  factory PinCodeSuggestionResponse.fromJson(Map<String, dynamic> json) => _$PinCodeSuggestionResponseFromJson(json);
}
