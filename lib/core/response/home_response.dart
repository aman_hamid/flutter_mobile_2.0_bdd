import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/order.dart';
import 'package:pocketpills/core/response/free_vitamins_response.dart';

part 'home_response.g.dart';

@JsonSerializable()
class HomeResponse {
  FreeVitaminsResponse freeVitamins;
  List<Order> lastOrder;
  bool hasFreeVitaminsOrder;
  bool hasPrescription;
  bool hasHealthInformation;
  bool hasInsurance;
  bool hasAddress;
  bool hasCreditCard;

  HomeResponse(
      {required this.freeVitamins,
      required this.lastOrder,
      required this.hasAddress,
      required this.hasFreeVitaminsOrder,
      required this.hasHealthInformation,
      required this.hasCreditCard,
      required this.hasPrescription,
      required this.hasInsurance});

  factory HomeResponse.fromJson(Map<String, dynamic> json) => _$HomeResponseFromJson(json);

  Map<String, dynamic> toJson() => _$HomeResponseToJson(this);
}
