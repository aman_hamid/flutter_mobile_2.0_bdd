import 'package:json_annotation/json_annotation.dart';
part 'payment_delete_response.g.dart';

@JsonSerializable()
class PaymentDeleteResponse {
  bool deleted;

  PaymentDeleteResponse({required this.deleted});

  factory PaymentDeleteResponse.fromJson(Map<String, dynamic> json) => _$PaymentDeleteResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentDeleteResponseToJson(this);
}
