import 'dart:io';

import 'package:async/async.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:pocketpills/core/apis/http_apis.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/insurance.dart';
import 'package:pocketpills/core/models/patient.dart';
import 'package:pocketpills/core/request/edit_insurance_request.dart';
import 'package:pocketpills/core/request/localization_request.dart';
import 'package:pocketpills/core/request/request_wrapper.dart';
import 'package:pocketpills/core/request/update_health_request.dart';
import 'package:pocketpills/core/response/base_response.dart';
import 'package:pocketpills/core/response/insurance/copy_insurance_candidate_response.dart';
import 'package:pocketpills/core/response/insurance_response.dart';
import 'package:pocketpills/core/response/old_base_response.dart';
import 'package:pocketpills/core/response/patient_health_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/locator.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class ProfileInsuranceModel extends BaseModel {
  final HttpApi _api = locator<HttpApi>();
  final DataStoreService dataStore = locator<DataStoreService>();

  AsyncMemoizer<Insurance?> _memoizer = AsyncMemoizer();
  AsyncMemoizer<Map<String, dynamic>?> contentMemoizer = AsyncMemoizer();

  int? prevPatientId;
  late Future<Insurance?> _futureInsurance;
  late List<Patient?> copyPrimaryInsuranceList;
  late List<Patient?> copySecondaryInsuranceList;

  Insurance? curInsurance;
  String curImageUploading = "";
  ConnectivityResult? connectivityResult;
  bool? fstHealth = false;
  bool? scndHealth = false;

  bool _secondaryInsurance = false, _tertiaryInsurance = false, _quaternaryInsurance = false, _allInsuranceAvailable = true;

  Future<Insurance?> fetchInsuranceData(int patientId) {
    if (prevPatientId != null && prevPatientId == patientId) return _futureInsurance;
    this._memoizer = AsyncMemoizer();
    _futureInsurance = this._memoizer.runOnce(() async {
      prevPatientId = patientId;
      curInsurance = await this.getPatientInsurance();
      return curInsurance;
    });
    return _futureInsurance;
  }

  Future<bool> setPatientInsurance(List<String> allergies, List<String> vitamins) async {
    setState(ViewState.Busy);
    Response response = await _api
        .setPatientInsurance(RequestWrapper(body: UpdateHealthRequest(allergies: allergies, vitamins: vitamins, patientId: dataStore.readInteger(DataStoreService.PATIENTID))));
    setState(ViewState.Idle);
    if (response != null) {
      BaseResponse<PatientHealthResponse> res = BaseResponse<PatientHealthResponse>.fromJson(response.data);
      if (!res.status!) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      prevPatientId = null;
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }

  clearAsyncMemorizer() {
    _memoizer = AsyncMemoizer();
    prevPatientId = null;
    notifyListeners();
  }

  Future<Insurance?> getPatientInsurance() async {
    connectivityResult = await checkInternet();
    if (connectivityResult == ConnectivityResult.none) {
      return null;
    }

    Response response = await _api.getPatientInsurance();
    if (response != null) {
      BaseResponse<InsuranceResponse> res = BaseResponse<InsuranceResponse>.fromJson(response.data);
      if (!res.status!) {
        errorMessage = res.getErrorMessage();
        return null;
      }
      curInsurance = res.response!.insurance;
      updateInsuranceUploadButton();
      notifyListeners();
      return curInsurance;
    } else {
      return null;
    }
  }

  void updateInsuranceUploadButton() {
    (curInsurance!.secondaryInsuranceFrontImage == null && curInsurance!.secondaryInsuranceBackImage == null) ? secondaryInsurance = false : secondaryInsurance = true;
    (curInsurance!.tertiaryInsuranceFrontImage == null && curInsurance!.tertiaryInsuranceBackImage == null) ? tertiaryInsurance = false : secondaryInsurance = true;
    (curInsurance!.quaternaryInsuranceFrontImage == null && curInsurance!.quaternaryInsuranceBackImage == null) ? quaternaryInsurance = false : secondaryInsurance = true;
  }

  addInsuranceImage(File image, String insuranceType, bool isInsuranceFront) async {
    Response response = await _api.addInsuranceImage(image, insuranceType, isInsuranceFront, dataStore.readInteger(DataStoreService.PATIENTID).toString());
    if (response != null) {
      if (response.statusCode == 413) {
        errorMessage = "Please reduce the size of your image";
        return false;
      }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      if (!res.status!) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      Insurance? ins = await getPatientInsurance();
      if (ins == null) return false;
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }

  addInsuranceHealthImage(File image, String insuranceType, bool isInsuranceFront) async {
    Response response = await _api.addInsuranceImage(image, insuranceType, isInsuranceFront, dataStore.readInteger(DataStoreService.PATIENTID).toString());
    if (response != null) {
      if (response.statusCode == 413) {
        errorMessage = "Please reduce the size of your image";
        return false;
      }
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      if (!res.status!) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }

  Future<String?> addImage(File image, String source) async {
    var uploadError = LocalizationUtils.getSingleValueString("insurance", "insurance.uploadmodal.document-error");
    var uploadSuccess = LocalizationUtils.getSingleValueString("insurance", "insurance.uploadmodal.document-success");
    switch (source) {
      case "provincialInsuranceFrontImage":
        curImageUploading = "provincialInsuranceFrontImage";
        notifyListeners();
        bool res = await addInsuranceImage(image, "PROVINCIAL", true);
        return handleUploadImageFile(res, uploadSuccess, uploadError,"");
      case "provincialInsuranceBackImage":
        curImageUploading = "provincialInsuranceBackImage";
        notifyListeners();
        bool res = await addInsuranceImage(image, "PROVINCIAL", false);
        return handleUploadImageFile(res, uploadSuccess, uploadError,"");
      case "primaryInsuranceFrontImage":
        curImageUploading = "primaryInsuranceFrontImage";
        notifyListeners();
        bool res = await addInsuranceImage(image, "PRIMARY", true);
        return handleUploadImageFile(res, uploadSuccess, uploadError,"");
      case "primaryInsuranceBackImage":
        curImageUploading = "primaryInsuranceBackImage";
        notifyListeners();

        bool res = await addInsuranceImage(image, "PRIMARY", false);
        return handleUploadImageFile(res, uploadSuccess, uploadError,"");
      case "secondaryInsuranceFrontImage":
        curImageUploading = "secondaryInsuranceFrontImage";
        notifyListeners();

        bool res = await addInsuranceImage(image, "SECONDARY", true);
        return handleUploadImageFile(res, uploadSuccess, uploadError,"");
      case "secondaryInsuranceBackImage":
        curImageUploading = "secondaryInsuranceBackImage";
        notifyListeners();
        bool res = await addInsuranceImage(image, "SECONDARY", false);
        return handleUploadImageFile(res, uploadSuccess, uploadError,"");

      case "tertiaryInsuranceFrontImage":
        curImageUploading = "tertiaryInsuranceFrontImage";
        notifyListeners();
        bool res = await addInsuranceImage(image, "TERTIARY", true);
        return handleUploadImageFile(res, uploadSuccess, uploadError,"");
      case "tertiaryInsuranceBackImage":
        curImageUploading = "tertiaryInsuranceBackImage";
        notifyListeners();
        bool res = await addInsuranceImage(image, "TERTIARY", false);
        return handleUploadImageFile(res, uploadSuccess, uploadError,"");
      case "quaternaryInsuranceFrontImage":
        curImageUploading = "quaternaryInsuranceFrontImage";
        notifyListeners();
        bool res = await addInsuranceImage(image, "QUATERNARY", true);
        return handleUploadImageFile(res, uploadSuccess, uploadError,"");
      case "quaternaryInsuranceBackImage":
        curImageUploading = "quaternaryInsuranceBackImage";
        notifyListeners();
        bool res = await addInsuranceImage(image, "QUATERNARY", false);
        return handleUploadImageFile(res, uploadSuccess, uploadError,"");
      case "provincialInsuranceHealthFrontImage":
        curImageUploading = "provincialInsuranceHealthFrontImage";
        notifyListeners();
        bool res = await addInsuranceHealthImage(image, "PROVINCIAL", true);
        return handleUploadImageFile(res, uploadSuccess, uploadError,"health1");
      case "provincialInsuranceHealthBackImage":
        curImageUploading = "provincialInsuranceHealthBackImage";
        notifyListeners();
        bool res = await addInsuranceHealthImage(image, "PROVINCIAL", false);
        return handleUploadImageFile(res, uploadSuccess, uploadError,"health2");
      default:
        return uploadError;
    }
  }

  String handleUploadImageFile(bool res, String uploadSuccess, String uploadError,String from) {
    if (res == true) {
      if(from == "health1") {
        fstHealth = true;
        if(fstHealth == true && scndHealth == true) {
          dataStore.writeInt(DataStoreService.STEPPER_CURRENT_STEP,1);
        } else {
          dataStore.writeInt(DataStoreService.STEPPER_CURRENT_STEP,0);
        }
      }
      if(from == "health2") {
        scndHealth = true;
        if(fstHealth == true && scndHealth == true) {
          dataStore.writeInt(DataStoreService.STEPPER_CURRENT_STEP,1);
        } else {
          dataStore.writeInt(DataStoreService.STEPPER_CURRENT_STEP,0);
        }
      }
      return notifyAfterUploadFile(uploadSuccess);
    }
    curImageUploading = "";
    notifyListeners();
    return uploadError;
  }

  String notifyAfterUploadFile(String uploadSuccess) {
    curImageUploading = "";
    prevPatientId = null;
    notifyListeners();
    return uploadSuccess;
  }

  deleteImage(String source) async {
    switch (source) {
      case "provincialInsuranceFrontImage":
        bool res = await deleteInsuranceImage("PROVINCIAL", true);
        if (res == true) {
          curInsurance!.provincialInsuranceFrontImage = null;
          curInsurance!.provincialInsuranceFrontImageOriginal = null;
          notifyListeners();
        }
        return;
      case "provincialInsuranceBackImage":
        bool res = await deleteInsuranceImage("PROVINCIAL", false);
        if (res == true) {
          curInsurance!.provincialInsuranceBackImage = null;
          curInsurance!.provincialInsuranceBackImageOriginal = null;
          notifyListeners();
        }
        return;
      case "primaryInsuranceFrontImage":
        bool res = await deleteInsuranceImage("PRIMARY", true);
        if (res == true) {
          curInsurance!.primaryInsuranceFrontImage = null;
          curInsurance!.primaryInsuranceFrontImageOriginal = null;
          notifyListeners();
        }
        return;
      case "primaryInsuranceBackImage":
        bool res = await deleteInsuranceImage("PRIMARY", false);
        if (res == true) {
          curInsurance!.primaryInsuranceBackImage = null;
          curInsurance!.primaryInsuranceBackImageOriginal = null;
          notifyListeners();
        }
        return;
      case "secondaryInsuranceFrontImage":
        bool res = await deleteInsuranceImage("SECONDARY", true);
        if (res == true) {
          curInsurance!.secondaryInsuranceFrontImage = null;
          curInsurance!.secondaryInsuranceFrontImageOriginal = null;
          notifyListeners();
        }
        return;
      case "secondaryInsuranceBackImage":
        bool res = await deleteInsuranceImage("SECONDARY", false);
        if (res == true) {
          curInsurance!.secondaryInsuranceBackImage = null;
          curInsurance!.secondaryInsuranceBackImageOriginal = null;
          notifyListeners();
        }
        return;
      case "tertiaryInsuranceFrontImage":
        bool res = await deleteInsuranceImage("TERTIARY", true);
        if (res == true) {
          curInsurance!.tertiaryInsuranceFrontImage = null;
          curInsurance!.tertiaryInsuranceFrontImageOriginal = null;
          notifyListeners();
        }
        return;
      case "tertiaryInsuranceBackImage":
        bool res = await deleteInsuranceImage("TERTIARY", false);
        if (res == true) {
          curInsurance!.tertiaryInsuranceBackImage = null;
          curInsurance!.tertiaryInsuranceBackImageOriginal = null;
          notifyListeners();
        }
        return;
      case "quaternaryInsuranceFrontImage":
        bool res = await deleteInsuranceImage("QUATERNARY", true);
        if (res == true) {
          curInsurance!.quaternaryInsuranceFrontImage = null;
          curInsurance!.quaternaryInsuranceFrontImageOriginal = null;
          notifyListeners();
        }
        return;
      case "quaternaryInsuranceBackImage":
        bool res = await deleteInsuranceImage("QUATERNARY", false);
        if (res == true) {
          curInsurance!.quaternaryInsuranceBackImage = null;
          curInsurance!.quaternaryInsuranceBackImageOriginal = null;
          notifyListeners();
        }
        return;
      default:
        return;
    }
  }

  Future<bool> getCopyInsuranceCandidate() async {
    setState(ViewState.Busy);
    Response response = await _api.getCopyInsuranceCandidate();
    setState(ViewState.Idle);
    if (response != null) {
      BaseResponse<CopyInsuranceCandidateResponse> res = BaseResponse<CopyInsuranceCandidateResponse>.fromJson(response.data);

      if (!res.status!) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      copyPrimaryInsuranceList = res.response!.primaryInsuranceCandidates;
      copySecondaryInsuranceList = res.response!.secondaryInsuranceCandidates;
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }

  Future<bool> getCopyInsuranceCardFromCandidate(int sourcePatientId, String fromInsuranceType, String toInsuranceType) async {
    setState(ViewState.Busy);
    Response insurenceResponse = await _api.getCopyInsuranceCardFromCandidate(sourcePatientId, fromInsuranceType, toInsuranceType);
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_copy_insurance);
    setState(ViewState.Idle);
    if (insurenceResponse != null) {
      OldBaseResponse res = OldBaseResponse.fromJson(insurenceResponse.data);
      errorMessage = res.getErrorMessage();

      await getPatientInsurance();

      if (!res.status!) {
        return false;
      }

      return true;
    } else {
      return false;
    }
  }

  Future<bool> deleteInsuranceImage(String insuranceType, bool isInsuranceFront) async {
    setState(ViewState.Busy);
    Response response = await _api.deleteInsuranceImage(RequestWrapper(
        body: EditInsuranceRequest(insuranceType: insuranceType, isInsuranceFront: isInsuranceFront, patientId: dataStore.readInteger(DataStoreService.PATIENTID).toString())));
    setState(ViewState.Idle);
    if (response != null) {
      OldBaseResponse res = OldBaseResponse.fromJson(response.data);
      if (!res.status!) {
        errorMessage = res.getErrorMessage();
        return false;
      }
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }

  get quaternaryInsurance => _quaternaryInsurance;

  set quaternaryInsurance(value) {
    _quaternaryInsurance = value;
    notifyListeners();
  }

  get tertiaryInsurance => _tertiaryInsurance;

  set tertiaryInsurance(value) {
    _tertiaryInsurance = value;
    notifyListeners();
  }

  bool get secondaryInsurance => _secondaryInsurance;

  set secondaryInsurance(bool value) {
    _secondaryInsurance = value;
    notifyListeners();
  }

  get allInsuranceAvailable => _allInsuranceAvailable;

  set allInsuranceAvailable(value) {
    _allInsuranceAvailable = value;
    notifyListeners();
  }

  Future<Map<String, dynamic>?> getLocalization(List<String> currentPage) async {
    return this.contentMemoizer.runOnce(() async {
      List<String>? keys = LocalizationUtils.getPageWiseDataList(currentPage);
      if (keys == null) {
        Map<String, dynamic>? localizedData = LocalizationUtils.getPageWiseData(currentPage[0]);
        print("Called shared preference $currentPage");
        return localizedData;
      } else {
        List<LocalizationRequest> sendList = <LocalizationRequest>[];
        keys.forEach((element) {
          LocalizationRequest request = LocalizationRequest(combinedIdentifierKey: element);
          sendList.add(request);
        });

        connectivityResult = await checkInternet();
        if (connectivityResult == ConnectivityResult.none) {
          return null;
        }
        var response = await _api.getLocalizationTexts2(sendList);
        if (response != null) {
          BaseResponse<Map> res = BaseResponse<Map>.fromJson(response.data);
          print("Called api  $keys");
          return LocalizationUtils.saveNewPageData2(keys, res.response);
        } else
          return null;
      }
    });
  }
}
