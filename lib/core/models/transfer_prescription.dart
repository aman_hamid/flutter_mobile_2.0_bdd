import 'package:json_annotation/json_annotation.dart';

part 'transfer_prescription.g.dart';

@JsonSerializable()
class TransferPrescription {
  int? id;
  String? pharmacyName;
  String? pharmacyAddress;
  int? pharmacyPhone;
  String? prescriptionState;
  String? type;
  int? omsPrescriptionId;
  int? appointmentTime;
  double? revenue;

  TransferPrescription(
      {this.id, this.prescriptionState, this.pharmacyName, this.pharmacyAddress, this.pharmacyPhone, this.omsPrescriptionId, this.type, this.appointmentTime, this.revenue});

  factory TransferPrescription.fromJson(Map<String, dynamic> json) => _$TransferPrescriptionFromJson(json);
}
