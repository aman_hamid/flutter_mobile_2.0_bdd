import 'package:json_annotation/json_annotation.dart';

part 'phone_verification.g.dart';

@JsonSerializable()
class PhoneVerification {
  String? hint;
  String? userMessage;
  String? redirect;
  String? signUpType;
  int? userId;
  PhoneVerification({this.hint, this.userMessage, this.redirect, this.signUpType, this.userId});

  factory PhoneVerification.fromJson(Map<String, dynamic> json) => _$PhoneVerificationFromJson(json);

  Map<String, dynamic> toJson() => _$PhoneVerificationToJson(this);
}
