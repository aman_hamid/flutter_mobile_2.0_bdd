import 'package:json_annotation/json_annotation.dart';
import 'package:pocketpills/core/models/transfer_prescription.dart';
import 'package:pocketpills/core/models/user.dart';

part 'auth_verification.g.dart';

@JsonSerializable()
class AuthVerification {
  String? hint;
  String? userMessage;
  String? redirect;
  String? userIdentifier;
  String? signUpType;
  int? userId;
  User? user;
  int? patientId;
  TransferPrescription? prescription;

  AuthVerification({this.hint, this.userMessage, this.redirect, this.signUpType, this.userId, this.user, this.patientId, this.prescription, this.userIdentifier});

  factory AuthVerification.fromJson(Map<String, dynamic> json) => _$AuthVerificationFromJson(json);

  Map<String, dynamic> toJson() => _$AuthVerificationToJson(this);
}
