class MedicalNeeds {
  String? autocompleteterm;
  MedicalNeeds({
    this.autocompleteterm,
  });

  factory MedicalNeeds.fromJson(Map<String, dynamic> parsedJson) {
    return MedicalNeeds(
      autocompleteterm: parsedJson['autocompleteTerm'] as String,
    );
  }
}
