// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tele_health_appointment_time.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppointmentTime _$AppointmentTimeFromJson(Map<String, dynamic> json) {
  return AppointmentTime(
    actualAppointmentTime:
        PPDateUtils.fromStr(json['actualAppointmentTime'] as String?),
    appointmentDate: json['appointmentDate'] as String?,
    available: json['available'] as bool?,
    clinicName: json['clinicName'] as String?,
    displayDate: json['displayDate'] as String?,
    doctorName: json['doctorName'] as String?,
    index: json['index'] as int?,
    timeSlot: json['timeSlot'] as String?,
  )..status = json['status'] as String?;
}

Map<String, dynamic> _$AppointmentTimeToJson(AppointmentTime instance) =>
    <String, dynamic>{
      'actualAppointmentTime':
          PPDateUtils.toStr(instance.actualAppointmentTime),
      'appointmentDate': instance.appointmentDate,
      'available': instance.available,
      'clinicName': instance.clinicName,
      'displayDate': instance.displayDate,
      'doctorName': instance.doctorName,
      'index': instance.index,
      'status': instance.status,
      'timeSlot': instance.timeSlot,
    };
