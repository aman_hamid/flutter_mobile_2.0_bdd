import 'package:json_annotation/json_annotation.dart';
part 'signup_dto.g.dart';

@JsonSerializable()
class SignupDto {
  int? phone;
  String? email;
  String? firstName;
  String? lastName;
  String? gender;
  String? password;
  String? birthDate;
  bool? hasDailyMedication;
  bool? hasHealthCard;
  bool? hasPassword;
  bool? maskedEmail;
  bool? isCaregiver;
  String? province;
  String? provider;
  bool? isMailVerified;
  String? locale;
  bool? verified;
  String? signUpType;
  String? prescriptionFlow;
  String? signupFlow;

  SignupDto(
      {this.password,
      this.phone,
      this.email,
      this.firstName,
      this.lastName,
      this.gender,
      this.hasPassword,
      this.maskedEmail,
      this.birthDate,
      this.hasDailyMedication,
      this.isCaregiver,
      this.province,
      this.isMailVerified,
      this.locale,
      this.verified,
      this.signUpType,
      this.prescriptionFlow,
      this.signupFlow,
      this.provider,
      this.hasHealthCard});
  factory SignupDto.fromJson(Map<String, dynamic> json) => _$SignupDtoFromJson(json);

  Map<String, dynamic> toJson() => _$SignupDtoToJson(this);
}
