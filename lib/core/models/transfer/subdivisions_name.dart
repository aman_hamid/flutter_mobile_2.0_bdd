import 'package:json_annotation/json_annotation.dart';
part 'subdivisions_name.g.dart';

@JsonSerializable()
class SubdivisionsName {
  String en;

  SubdivisionsName(this.en);

  factory SubdivisionsName.fromJson(Map<String, dynamic> json) => _$SubdivisionsNameFromJson(json);
}
