// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transfer_prescription.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransferPrescription _$TransferPrescriptionFromJson(Map<String, dynamic> json) {
  return TransferPrescription(
    id: json['id'] as int?,
    prescriptionState: json['prescriptionState'] as String?,
    pharmacyName: json['pharmacyName'] as String?,
    pharmacyAddress: json['pharmacyAddress'] as String?,
    pharmacyPhone: json['pharmacyPhone'] as int?,
    omsPrescriptionId: json['omsPrescriptionId'] as int?,
    type: json['type'] as String?,
    appointmentTime: json['appointmentTime'] as int?,
    revenue: (json['revenue'] as num?)?.toDouble(),
  );
}

Map<String, dynamic> _$TransferPrescriptionToJson(
        TransferPrescription instance) =>
    <String, dynamic>{
      'id': instance.id,
      'pharmacyName': instance.pharmacyName,
      'pharmacyAddress': instance.pharmacyAddress,
      'pharmacyPhone': instance.pharmacyPhone,
      'prescriptionState': instance.prescriptionState,
      'type': instance.type,
      'omsPrescriptionId': instance.omsPrescriptionId,
      'appointmentTime': instance.appointmentTime,
      'revenue': instance.revenue,
    };
