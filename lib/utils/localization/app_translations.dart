import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';

class AppTranslations {
  late Locale locale;

  AppTranslations(Locale locale) {
    this.locale = locale;
  }

  static AppTranslations? of(BuildContext context) {
    return Localizations.of<AppTranslations>(context, AppTranslations);
  }

  static Future<AppTranslations> load(Locale locale) async {
    AppTranslations appTranslations = AppTranslations(locale);
    return appTranslations;
  }
}
