import 'package:permission_handler/permission_handler.dart';

class PermissionUtils {
  Future<bool> requestPermission(Permission permission) async {
    PermissionStatus _permissionStatus = PermissionStatus.restricted;

    final List<Permission> permissions = <Permission>[permission];
    final Map<Permission, PermissionStatus> permissionRequestResult = await permissions.request();

    _permissionStatus = permissionRequestResult[permission]!;

    if (_permissionStatus == PermissionStatus.granted) {
      return true;
    } else {
      return false;
    }
  }

  Future<PermissionStatus> requestPermissionWithStatus(Permission permission) async {
    PermissionStatus _permissionStatus = PermissionStatus.restricted;

    final List<Permission> permissions = <Permission>[permission];
    final Map<Permission, PermissionStatus> permissionRequestResult = await permissions.request();

    _permissionStatus = permissionRequestResult[permission]!;

    return _permissionStatus;
  }
}
