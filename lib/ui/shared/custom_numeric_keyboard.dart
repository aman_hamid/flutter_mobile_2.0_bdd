import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';

class PPDoneButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        color: lightBlue,
        child: Align(
            alignment: Alignment.topRight,
            child: Padding(
                padding: EdgeInsets.only(top: 4.0, bottom: 4.0),
                child: OutlineButton(
                  child: Text(
                    "Done",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                  },
                ))));
  }
}
