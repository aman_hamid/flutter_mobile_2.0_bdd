import 'package:flutter/material.dart';

import '../../res/colors.dart';

class PPDivider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Divider(
      height: 1.0,
      color: borderColor,
      thickness: 1,
    );
  }
}
