import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:launch_review/launch_review.dart';
import 'package:pocketpills/core/response/version_response.dart';
import 'package:pocketpills/core/viewmodels/base_model.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/views/base_stateless_widget.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:url_launcher/url_launcher.dart';

class UpdateDialog extends BaseStatelessWidget {
  final VersionResponse? versionResponse;

  UpdateDialog({this.versionResponse});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        LocalizationUtils.getSingleValueString("common", "common.label.new-update"),
        style: REGULAR_XXX_PRIMARY_BOLD,
      ),
      content: Linkify(
        onOpen: (link) async {
          if (await canLaunch(link.url)) {
            await launch(link.url);
          } else {
            throw 'Could not launch $link';
          }
        },
        text: versionResponse!.errMessage,
        style: MEDIUM_XXX_SECONDARY,
        linkStyle: MEDIUM_XXX_LINK_BOLD_MEDIUM,
      ),
      actions: <Widget>[
        versionResponse!.skippable == true
            ? FlatButton(
                child: Text(
                  LocalizationUtils.getSingleValueString("common", "common.button.cancel"),
                  style: MEDIUM_XXX_PRIMARY_BOLD,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            : PPContainer.emptyContainer(),
        FlatButton(
          child: Text(
            LocalizationUtils.getSingleValueString("common", "common.button.update"),
            style: MEDIUM_XXX_PRIMARY_BOLD,
          ),
          onPressed: () {
            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_update_application);
            LaunchReview.launch(androidAppId: "com.pocketpills", iOSAppId: "1367442074", writeReview: false);
          },
        )
      ],
    );
  }

  static Function show(BuildContext context, VersionResponse versionResponse) {
    return () {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return UpdateDialog(
            versionResponse: versionResponse,
          );
        },
      );
    };
  }
}
