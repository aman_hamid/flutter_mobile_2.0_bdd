import 'package:flutter/material.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';

class LoadingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Center(child: ViewConstants.progressIndicator),
      ),
    );
  }
}
