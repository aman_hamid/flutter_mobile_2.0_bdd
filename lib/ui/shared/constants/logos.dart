import 'package:flutter/material.dart';

class Logos {
  static const String ppLogoPath = 'graphics/ppLogo.png';
  static const String ppHorizontalLogoPath = 'graphics/logo-horizontal-dark.png';
  static getBigLogo(){
    return Row(
      children: <Widget>[
        Image.asset(
          ppLogoPath,
          width: 56,
          height: 56,
        ),
      ],
    );
  }
}
