import 'package:contacts_service/contacts_service.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pocketpills/core/response/referral_response.dart';
import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/buttons/transparent_button.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/dialogs/permission_dialog.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_divider.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_horizontal_line.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/dashboard/dashboard_view.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/utils/http_api_utils.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/permission_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';

class ReferralContactSyncView extends StatefulWidget {
  static const routeName = 'referralContactSync';

  @override
  _ReferralContactSyncViewState createState() => _ReferralContactSyncViewState();
}

class _ReferralContactSyncViewState extends BaseState<ReferralContactSyncView> {
  final Permission _permissionGroup = Permission.contacts;
  PermissionStatus _permissionStatus = PermissionStatus.restricted;
  final TextEditingController _searchContactController = TextEditingController();
  final FocusNode _seachContactFnode = FocusNode();
  List<Contact>? _contacts;
  List<Contact>? _seachResult;
  Map<String, Contact> _sendMessageMap = Map();

  @override
  void initState() {
    super.initState();
    HomeModel model = HomeModel();
    getLanguageData(model);
    _searchContactController.addListener(seachContactListener);
    checkPhoneNumberPermission();
  }

  @override
  void dispose() {
    _searchContactController.removeListener(seachContactListener);
    super.dispose();
  }

  checkPhoneNumberPermission() async {
    PermissionStatus permission = await Permission.contacts.status;
    if (permission == PermissionStatus.granted) {
      _listenForPermissionStatus();
    }
  }

  seachContactListener() {
    List<Contact> _finalSeachResult = [];

    _contacts!.forEach((contact) {
      if (contact.displayName != null && contact.displayName != "" && contact.displayName!.toLowerCase().contains(_searchContactController.text)) {
        _finalSeachResult.add(contact);
      }
    });

    setState(() {
      _seachResult = _finalSeachResult;
    });
  }

  void _listenForPermissionStatus() async {
    await PermissionUtils().requestPermissionWithStatus(_permissionGroup).then((PermissionStatus status) {
      setState(() {
        _permissionStatus = status;
        if (status == PermissionStatus.granted) {
          refreshContacts();
        }
      });
    });
  }

  refreshContacts() async {
    PermissionStatus permissionStatus = await PermissionUtils().requestPermissionWithStatus(_permissionGroup);
    if (permissionStatus == PermissionStatus.granted) {
      var contacts = (await ContactsService.getContacts(withThumbnails: false)).toList();
      setState(() {
        _contacts = contacts;
        _seachResult = contacts;
      });

      if (_contacts != null) {
        await HttpApiUtils().postUserContact(contacts);
      }
    } else {
      _handleInvalidPermissions(permissionStatus);
    }
  }

  void _handleInvalidPermissions(PermissionStatus permissionStatus) async {
    if (permissionStatus == PermissionStatus.denied) {
      FirebaseCrashlytics.instance.log("PERMISSION_DENIED");
    } else if (permissionStatus == PermissionStatus.permanentlyDenied) {
      FirebaseCrashlytics.instance.log("PERMISSION_DISABLED");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<HomeModel>(
      builder: (BuildContext context, HomeModel homeModel, Widget? child) {
        return FutureBuilder(
          future: homeModel.fetchReferralData(Provider.of<DashboardModel>(context).selectedPatientId),
          builder: (BuildContext context, AsyncSnapshot<ReferralResponse?> snapshot) {
            if (snapshot.hasData != null && snapshot.data != null) {
              return Scaffold(
                  appBar: InnerAppBar(
                      titleText: LocalizationUtils.getSingleValueString("referral", "referral.all.refer-friend"),
                      appBar: AppBar(),
                      leadingBackButton: () {
                        Navigator.of(context).pushNamedAndRemoveUntil(DashboardWidget.routeName, (Route<dynamic> route) => false);
                      }),
                  body: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          decoration: BoxDecoration(color: Color(0xfff7f7f7)),
                          child: Padding(
                              padding: EdgeInsets.all(PPUIHelper.HorizontalSpaceMedium),
                              child: Column(children: <Widget>[
                                SizedBox(
                                  height: SMALL_XXX,
                                ),
                                (snapshot.requireData != null && snapshot.requireData!.referralCode != null)
                                    ? getReferralContainer(context, snapshot.requireData!.referralCode!)
                                    : Container(),
                                SizedBox(
                                  height: SMALL_XXX,
                                ),
                              ]))),
                      _permissionStatus == PermissionStatus.granted
                          ? Padding(
                              padding: const EdgeInsets.only(top: MEDIUM_XXX, left: MEDIUM_XXX, right: MEDIUM_XXX),
                              child: PPFormFields.getTextField(
                                  keyboardType: TextInputType.emailAddress,
                                  focusNode: _seachContactFnode,
                                  controller: _searchContactController,
                                  textInputAction: TextInputAction.next,
                                  decoration: PPInputDecor.getDecoration(
                                      labelText: LocalizationUtils.getSingleValueString("referral", "referral.all.search-contact"),
                                      hintText: LocalizationUtils.getSingleValueString("referral", "referral.all.enter-name"),
                                      suffixIcon: Icon(Icons.search)),
                                  onFieldSubmitted: (value) {}),
                            )
                          : Container(),
                      getContactListView(snapshot.requireData!.referralCode!, homeModel)
                    ],
                  ));
            } else if (snapshot.hasError) {
              return ErrorScreen();
            } else {
              return LoadingScreen();
            }
          },
        );
      },
    );
  }

  Widget getSeparateDivider() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        CustomPaint(painter: PPhorizontalline(true, (MediaQuery.of(context).size.width / 2) - 24)),
        Text(
          LocalizationUtils.getSingleValueString("referral", "referral.all.or"),
          style: TextStyle(color: primaryColor, fontWeight: FontWeight.bold, fontSize: PPUIHelper.FontSizeLarge),
        ),
        CustomPaint(painter: PPhorizontalline(false, (MediaQuery.of(context).size.width / 2) - 24))
      ],
    );
  }

  Widget getContactListView(String referralCode, HomeModel homeModel) {
    if (_permissionStatus == PermissionStatus.granted) {
      return _seachResult != null
          ? Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  primary: false,
                  itemCount: _seachResult?.length ?? 0,
                  itemBuilder: (BuildContext context, int index) {
                    Contact? c = _seachResult?.elementAt(index);
                    return Container(
                        constraints: BoxConstraints(maxWidth: double.infinity, maxHeight: double.infinity),
                        child: c!.phones!.length > 0
                            ? Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(top: SMALL_XX, left: MEDIUM_XXX, bottom: SMALL_XX),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              c.displayName ?? "",
                                              style: MEDIUM_XXX_PRIMARY_BOLD,
                                            ),
                                            SizedBox(
                                              height: SMALL_X,
                                            ),
                                            ItemsTile(c.phones!),
                                          ],
                                        ),
                                        (_sendMessageMap.containsKey(c.phones!.elementAt(0).value) == true)
                                            ? Padding(
                                                padding: const EdgeInsets.only(right: LARGE_X, top: MEDIUM_X, bottom: MEDIUM_X),
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.all(Radius.circular(REGULAR_XX)),
                                                    boxShadow: [
                                                      BoxShadow(
                                                        color: Colors.black12,
                                                        blurRadius: REGULAR,
                                                      ),
                                                    ],
                                                    color: successColor,
                                                  ),
                                                  child: Padding(
                                                    padding: const EdgeInsets.all(4.0),
                                                    child: Icon(
                                                      Icons.check,
                                                      color: whiteColor,
                                                      size: 16,
                                                    ),
                                                  ),
                                                ),
                                              )
                                            : TransparentButton(
                                                text: LocalizationUtils.getSingleValueString("referral", "referral.all.refer"),
                                                bgColor: Colors.white,
                                                onPressed: () async {
                                                  try {
                                                    bool result = await homeModel.postSendInvitationSMS(c.phones!.elementAt(0).value!);
                                                    if (result == true) {
                                                      setState(() {
                                                        _sendMessageMap[c.phones!.elementAt(0).value!] = c;
                                                      });
                                                      onFail(context, errMessage: homeModel.errorMessage);
                                                    }
                                                  } catch (ex) {
                                                    FirebaseCrashlytics.instance.log(ex.toString());
                                                  }
                                                  analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_referral_refer);
                                                },
                                              )
                                      ],
                                    ),
                                  ),
                                  PPDivider()
                                ],
                              )
                            : Container());
                  }),
            )
          : Expanded(
              child: Center(
                child: ViewConstants.progressIndicator,
              ),
            );
    } else {
      return Padding(
        padding: const EdgeInsets.all(MEDIUM_XXX),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: REGULAR_XXX,
            ),
            getSeparateDivider(),
            SizedBox(
              height: REGULAR_XXX,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: RaisedButton(
                    color: Colors.white,
                    disabledTextColor: Colors.white,
                    shape: RoundedRectangleBorder(
                      side: BorderSide(color: tertiaryColor),
                      borderRadius: BorderRadius.circular(SMALL_XX),
                    ),
                    child: Text(
                      LocalizationUtils.getSingleValueString("referral", "referral.all.sync-contact"),
                      style: MEDIUM_XXX_LINK_BOLD_MEDIUM,
                    ),
                    onPressed: () async {
                      PermissionStatus permmissionStatus = await PermissionUtils().requestPermissionWithStatus(Permission.contacts);
                      setState(() {
                        _permissionStatus = permmissionStatus;
                      });
                      if (permmissionStatus == PermissionStatus.granted) {
                        refreshContacts();
                      } else {
                        PermissionDialog.show(context, "contacts");
                      }
                      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_referral_sync_contact);
                    },
                  ),
                ),
              ],
            ),
            SizedBox(
              height: MEDIUM_XXX,
            ),
            Text(
              LocalizationUtils.getSingleValueString("referral", "referral.all.offer-message"),
              style: MEDIUM_XX_SECONDARY,
            )
          ],
        ),
      );
    }
  }

  Widget getReferralContainer(BuildContext context, String referralCode) {
    return Container(
      decoration: BoxDecoration(color: Colors.white, shape: BoxShape.rectangle, borderRadius: BorderRadius.all(Radius.circular(4.0))),
      child: DottedBorder(
          dashPattern: [6, 4],
          strokeWidth: 1,
          color: brandColor,
          child: Padding(
              padding: EdgeInsets.only(left: PPUIHelper.HorizontalSpaceLarge, right: PPUIHelper.HorizontalSpacexSmall),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Text(
                      referralCode,
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: brandColor, fontSize: PPUIHelper.FontSizeXLarge, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      TransparentButton(
                        text: LocalizationUtils.getSingleValueString("referral", "referral.all.copy"),
                        bgColor: Colors.white,
                        onPressed: () {
                          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_referral_code_copy);
                          Clipboard.setData(new ClipboardData(text: referralCode));
                          showOnSnackBar(context, successMessage: LocalizationUtils.getSingleValueString("referral", "referral.all.copied-clipboard"));
                        },
                      ),
                      TransparentButton(
                        text: LocalizationUtils.getSingleValueString("referral", "referral.all.share"),
                        bgColor: Colors.white,
                        onPressed: () {
                          analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_referral_code_share);
                          final RenderBox box = context.findRenderObject() as RenderBox;
                          Share.share(StringUtils.referralShareMessage(referralCode),
                              subject: ReferralModuleConstant.referralSharedMessageTitle, sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
                        },
                      )
                    ],
                  ),
                ],
              ))),
    );
  }
}

class ItemsTile extends StatelessWidget {
  ItemsTile(this._items);

  final Iterable<Item> _items;

  @override
  Widget build(BuildContext context) {
    if (_items.length > 0) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(_items.elementAt(0).value ?? "", style: MEDIUM_XX_SECONDARY),
        ],
      );
    } else {
      return PPContainer.emptyContainer();
    }
  }
}

void getLanguageData(HomeModel model) {
  FutureBuilder(
      future: myFutureMethodOverall(model),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData != null && snapshot.data != null) {
          return snapshot.data;
        } else if (snapshot.hasError) {
          return ErrorScreen();
        } else {
          return LoadingScreen();
        }
      });
}

Future myFutureMethodOverall(HomeModel model) async {
  Future<Map<String, dynamic>?> future1 = model.getLocalization(["referral"]);
  return await Future.wait([future1]);
}
