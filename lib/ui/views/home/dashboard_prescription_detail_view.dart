import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/models/response/prescription/prescription.dart';
import 'package:pocketpills/core/viewmodels/dashboard/prescription_dashboard_model.dart';
import 'package:pocketpills/ui/shared/appbars/inner_appbar.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/no_internet_screen.dart';
import 'package:pocketpills/ui/views/prescriptions/prescription_detail_view.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:provider/provider.dart';

class DashboardPrescriptionsDetailsView extends StatefulWidget {
  static const routeName = 'dashboardPrescriptionsDetailsView';
  final int? prescriptionId;

  DashboardPrescriptionsDetailsView({this.prescriptionId});

  @override
  State<StatefulWidget> createState() {
    return DashboardPrescriptionsDetailsViewState();
  }
}

class DashboardPrescriptionsDetailsViewState extends BaseState<DashboardPrescriptionsDetailsView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [ChangeNotifierProvider<PrescriptionDashboardModel>(create: (_) => PrescriptionDashboardModel())],
        child: Consumer<PrescriptionDashboardModel>(
          builder: (BuildContext context, PrescriptionDashboardModel prescriptionDashboardModel, Widget? child) {
            return Scaffold(
              appBar: InnerAppBar(
                titleText: LocalizationUtils.getSingleValueString("modal", "modal.prescriptiondetails.title"),
                appBar: AppBar(),
              ),
              body: FutureBuilder(
                future: myFutureMethodOverall(prescriptionDashboardModel),
                builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                  if (prescriptionDashboardModel.connectivityResult == ConnectivityResult.none && snapshot.connectionState == ConnectionState.done) {
                    return NoInternetScreen(
                      onClickRetry: prescriptionDashboardModel.clearAsyncMemoizer,
                    );
                  }

                  if (snapshot.hasData && prescriptionDashboardModel.connectivityResult != ConnectivityResult.none) {
                    return afterFutureResolved(context, snapshot.data[0]);
                  } else if (snapshot.hasError && prescriptionDashboardModel.connectivityResult != ConnectivityResult.none) {
                    FirebaseCrashlytics.instance.log(snapshot.hasError.toString());
                    return ErrorScreen();
                  }

                  if (snapshot.connectionState == ConnectionState.active || snapshot.connectionState == ConnectionState.waiting) {
                    return LoadingScreen();
                  }
                  return PPContainer.emptyContainer();
                },
              ),
            );
          },
        ));
  }

  Widget afterFutureResolved(BuildContext context, Prescription prescription) {
    return PrescriptionDetailWidget().getPrescriptionDetailView(prescription);
  }

  Future myFutureMethodOverall(PrescriptionDashboardModel prescriptionModel) async {
    Future<Prescription?> future1 = prescriptionModel.fetchPrescriptionById(widget.prescriptionId!);
    Future<Map<String, dynamic>?> future2 = prescriptionModel.getLocalization(["prescriptions"]);
    return await Future.wait([future1, future2]);
  }
}
