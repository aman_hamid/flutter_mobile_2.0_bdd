import 'package:pocketpills/core/models/response/prescription/prescription.dart';

class PrescriptionDetailsArguments {
  final Prescription? prescription;

  PrescriptionDetailsArguments({this.prescription});
}
