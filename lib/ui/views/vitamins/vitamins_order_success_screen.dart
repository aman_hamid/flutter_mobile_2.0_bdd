import 'package:flutter/material.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/success/base_success_screen.dart';
import 'package:pocketpills/ui/shared/success/transaction_success_enums.dart';
import 'package:pocketpills/ui/views/base_stateless_widget.dart';

class VitaminsOrderSuccessScreen extends BaseStatelessWidget {
  static const String routeName = 'vitaminsOrderSuccessScreen';

  final BaseStepperSource? source;

  VitaminsOrderSuccessScreen({this.source});

  @override
  Widget build(BuildContext context) {
    return BaseSuccessScreen(
      source: BaseStepperSource.VITAMINS_SCREEN,
      transactionSuccessEnum: TransactionSuccessEnum.VITAMINS_SUCCESS,
    );
  }
}
