import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/res/text_style.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

class NoInternetScreen extends StatelessWidget {
  final String title;
  final String description;
  final bool showTransfer;
  final String imageLink;
  final Function? onClickRetry;

  NoInternetScreen({this.imageLink = "graphics/no_internet.png", this.title = "Title", this.description = "Description", this.showTransfer = false, this.onClickRetry});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: PPUIHelper.HorizontalSpaceMedium),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      "graphics/no_internet.png",
                      width: MediaQuery.of(context).size.height * 0.4,
                      height: MediaQuery.of(context).size.height * 0.4,
                    ),
                  ],
                ),
                SizedBox(
                  height: LARGE_X,
                ),
                Text(
                  LocalizationUtils.getSingleValueString("common", "common.label.no-internet"),
                  style: REGULAR_XXX_PRIMARY_BOLD,
                ),
                SizedBox(
                  height: SMALL_XX,
                ),
                Text(
                  LocalizationUtils.getSingleValueString("common", "common.label.check-internet"),
                  style: MEDIUM_XXX_SECONDARY,
                ),
                SizedBox(
                  height: REGULAR_XXX,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RaisedButton.icon(
                      color: brandColor,
                      disabledTextColor: Colors.white,
                      shape: RoundedRectangleBorder(
                        side: BorderSide(color: brandColor),
                        borderRadius: BorderRadius.circular(SMALL_XX),
                      ),
                      icon: Padding(
                        padding: const EdgeInsets.only(left: MEDIUM_XXX),
                        child: Icon(
                          Icons.refresh,
                          color: whiteColor,
                        ),
                      ),
                      label: Padding(
                        padding: const EdgeInsets.only(left: SMALL, right: MEDIUM_XXX),
                        child: Text(LocalizationUtils.getSingleValueString("common", "common.button.retry"), style: MEDIUM_XXX_WHITE),
                      ),
                      onPressed: () {
                        onClickRetry!();
                      },
                    ),
                  ],
                )
              ],
            ),
          ),
        )),
      ),
    );
  }
}
