import 'dart:convert';
import 'dart:io';
import 'package:android_external_storage/android_external_storage.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf_render/pdf_render_widgets.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:path/path.dart' as path;
import 'package:provider/provider.dart';

class PDFViewer extends StatefulWidget {
  final String? pdfUrl;
  final bool? order;
  late SignUpModel model;

  PDFViewer({this.pdfUrl, this.order = false});

  @override
  _PDFViewer createState() => _PDFViewer(pdfUrl);
}

class _PDFViewer extends State<PDFViewer> {
  String? pdfUrl;
  bool downloading = false;
  double download = 0.0;
  String downloadingStr = "No data";
  FlutterLocalNotificationsPlugin? flutterLocalNotificationsPlugin;
  bool flagDueNotificationIssue = true;

  _PDFViewer(this.pdfUrl);

  late SignUpModel model;

  @override
  void initState() {
    super.initState();
    model = SignUpModel();
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    final android = AndroidInitializationSettings('@mipmap/ic_launcher');
    final iOS = IOSInitializationSettings();
    final initSettings = InitializationSettings(android: android, iOS: iOS);
    flutterLocalNotificationsPlugin!.initialize(initSettings, onSelectNotification: _onSelectNotification);
  }

  @override
  void dispose() {
    super.dispose();
    flutterLocalNotificationsPlugin!.cancelAll();
  }

  Future<void> _showNotification(Map<String, dynamic> downloadStatus) async {
    final android = AndroidNotificationDetails('channel id', 'channel name', 'channel description', priority: Priority.high, importance: Importance.max);
    final iOS = IOSNotificationDetails();
    final platform = NotificationDetails(android: android, iOS: iOS);
    final json = jsonEncode(downloadStatus);
    final isSuccess = downloadStatus['isSuccess'];
    await flutterLocalNotificationsPlugin!.show(
        0, // notification id
        isSuccess ? 'Success' : 'Failure',
        isSuccess ? 'File has been downloaded successfully!' : 'There was an error while downloading the file.',
        platform,
        payload: json);
  }

  Future<void> _onSelectNotification(String? json) async {
    final obj = jsonDecode(json!);
    if (Platform.isAndroid && flagDueNotificationIssue) {
      return;
    }
    if (obj['isSuccess']) {
      OpenFile.open(obj['filePath']);
    } else {
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: Text(LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.pdf.error",)),
          content: Text('${obj['error']}'),
        ),
      );
    }
  }

  Future<String?> _getStorageDirectory() async {
    return await AndroidExternalStorage.getExternalStoragePublicDirectory(DirType.downloadDirectory);
  }

  Future<Directory> _getDownloadDirectory() async {
    return await getApplicationDocumentsDirectory();
  }

  Future<bool> _requestPermissions() async {
    var permission = await Permission.storage.status;
    if (permission != PermissionStatus.granted) {
      await Permission.storage.request();
      permission = await Permission.storage.status;
    }
    return permission == PermissionStatus.granted;
  }

  @override
  Widget build(BuildContext context) {
    print("pdfurl ===   " + widget.pdfUrl.toString());
    return FutureBuilder(
        future: model.getLocalization(["prescriptions"]),
        builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>?> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            return getMainView();
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Widget getMainView() {
    return Scaffold(
      appBar: AppBar(
        title: Text(LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.pdf.title")),
        backgroundColor: Colors.black,
        actions: widget.order == true
            ? <Widget>[
                LocalizationUtils.isProvinceQuebec()
                    ? Container()
                    : Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: GestureDetector(
                          onTap: () {
                            downloadFile(pdfUrl!);
                          },
                          child: Icon(
                            Icons.download_rounded,
                            size: PPUIHelper.ICONSizeXSmall,
                          ),
                        ),
                      )
              ]
            : <Widget>[
                SizedBox(
                  width: 0.0,
                  height: 0.0,
                ),
              ],
      ),
      body: FutureBuilder<String>(
        future: downloadPdfFile(pdfUrl!),
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          Text text = Text('');
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              text = Text(LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.pdf.error") + ': ${snapshot.error}',style: TextStyle(fontFamily: "FSJoeyPro Bold"),);
            } else if (snapshot.hasData) {
              text = Text(LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.pdf.data") + ': ${snapshot.data}',style: TextStyle(fontFamily: "FSJoeyPro Bold"),);
              return PdfViewer.openFile(snapshot.data!);
            } else {
              text = Text(LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.pdf.unavailable"));
            }
          } else if (snapshot.connectionState == ConnectionState.waiting) {
            text = Text(LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.pdf.downloading"),style: TextStyle(fontFamily: "FSJoeyPro Bold"),);
          } else {
            text = Text(LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.pdf.loading-pdf"));
          }
          return downloading
              ? Center(
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        downloading = false;
                      });
                    },
                    child: Container(
                      height: 250,
                      width: 250,
                      child: Card(
                        color: brandColor,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircularProgressIndicator(
                              backgroundColor: Colors.white,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              downloadingStr,
                              style: TextStyle(color: Colors.white),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              : Container(
                  child: text,
                );
        },
      ),
    );
  }

  Future downloadFile(String url) async {
    String downloadPath = "";
    Directory? dir;
    try {
      if (Platform.isAndroid) {
        downloadPath = (await _getStorageDirectory())!;
      } else {
        dir = await _getDownloadDirectory();
      }
      final isPermissionStatusGranted = await _requestPermissions();
      final filename = "pocketpillsOrderReceipt" + new DateTime.now().millisecondsSinceEpoch.toString() + '.pdf';
      if (isPermissionStatusGranted) {
        String savePath;
        if (Platform.isAndroid) {
          savePath = path.join(downloadPath, filename);
        } else {
          savePath = path.join(dir!.path, filename);
        }
        await _startDownload(url, savePath);
      } else {
        // handle the scenario when user declines the permissions
      }
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> _startDownload(String url, String savePath) async {
    Map<String, dynamic> result = {
      'isSuccess': false,
      'filePath': null,
      'error': null,
    };
    final Dio dio = Dio();
    try {
      final response = await dio.download(url, savePath, onReceiveProgress: _onReceiveProgress);
      if (response.statusCode == 200)
        setState(() {
          downloadingStr = 'Completed';
          flagDueNotificationIssue = false;
        });
      result['isSuccess'] = response.statusCode == 200;
      result['filePath'] = savePath;
    } catch (ex) {
      result['error'] = ex.toString();
    } finally {
      await _showNotification(result);
    }
  }

  void _onReceiveProgress(int received, int total) {
    if (total != -1) {
      setState(() {
        downloading = true;
        download = (received / total) * 100;
        downloadingStr = LocalizationUtils.getSingleValueString("prescriptions", "prescriptions.pdf.downloading") + (download).toStringAsFixed(0);
      });
    }
  }

  Future<String> downloadPdfFile(String url) async {
    final filename = "pocketpillsOrderReceipt" + new DateTime.now().millisecondsSinceEpoch.toString();
    String dir = (await getTemporaryDirectory()).path;
    File file = new File('$dir/$filename');
    bool exist = false;
    try {
      await file.length().then((len) {
        exist = true;
      });
    } catch (e) {
      print(e);
    }
    if (!exist) {
      var request = await HttpClient().getUrl(Uri.parse(url));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);
      await file.writeAsBytes(bytes);
    }
    return file.path;
  }
}
