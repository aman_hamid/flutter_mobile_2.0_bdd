import 'package:pocketpills/ui/base/base_stepper_arguments.dart';

class SignUpVerificationArguments {
  final String? identifier;
  final BaseStepperSource? source;

  SignUpVerificationArguments({this.identifier, this.source});
}
