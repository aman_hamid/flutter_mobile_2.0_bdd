import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';

class VerificationArguments {
  final String? userIdentifier;
  final BaseStepperSource? source;
  final SignupStepperArguments? signupStepperArguments;

  VerificationArguments({this.userIdentifier, this.source, this.signupStepperArguments});
}
