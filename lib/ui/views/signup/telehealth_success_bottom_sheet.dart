import 'package:flutter/material.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/ui/views/error_screen.dart';

import 'package:pocketpills/core/utils/string_constant.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/views/orderstepper/order_stepper.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';

import 'package:pocketpills/ui/shared/constants/view_constants.dart';

class SuccessSheet extends StatefulWidget {
  BuildContext context;
  String openFrom = "";

  SuccessSheet(this.context, this.openFrom);

  _SuccessSheetState createState() => _SuccessSheetState();
}

class _SuccessSheetState extends BaseState<SuccessSheet> with SingleTickerProviderStateMixin {
  SignUpTransferModel? model;
  late BuildContext context;

  _SuccessSheetState();

  @override
  void initState() {
    super.initState();
    model = SignUpTransferModel();
    context = widget.context;
    print("openfrom" + widget.openFrom);
  }

  @override
  void didChangeDependencies() {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.signup_success);
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.consultation_signup_success);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext buildContext) {
    return FutureBuilder(
        future: myFutureMethodOverall(model!),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            dataStore.writeBoolean(DataStoreService.NEW_USER_SEARCH, false);
            return getMainView(buildContext);
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Future myFutureMethodOverall(SignUpTransferModel model) async {
    Future<Map<String, dynamic>?> future1 = model.getLocalization(["signup", "modal"]);
    return await Future.wait([future1]);
  }

  Widget getMainView(BuildContext context) {
    return SafeArea(
      child: Wrap(
        children: [
          Container(
            child: Column(
              children: [
                Container(
                  height: 45,
                  child: Stack(
                    alignment: Alignment.bottomCenter,
                    clipBehavior: Clip.none,
                    children: [
                      Container(
                        decoration: new BoxDecoration(
                            color: brandColor, //new Color.fromRGBO(255, 0, 0, 0.0),
                            borderRadius: new BorderRadius.only(topLeft: const Radius.circular(10.0), topRight: const Radius.circular(10.0))),
                      ),
                      Positioned(
                        top: -40,
                        child: CircleAvatar(
                            radius: 40,
                            backgroundColor: lightBlueColor,
                            child: CircleAvatar(
                              radius: 36,
                              backgroundColor: brandColor,
                              backgroundImage: CachedNetworkImageProvider(
                                "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg",
                              ),
                            )),
                      ),
                    ],
                  ),
                ),
                Container(
                  color: whiteColor,
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        color: brandColor,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            getTitleView(),
                            SizedBox(height: 10.0),
                            getSubTitleView(),
                            SizedBox(height: 20),
                          ],
                        ),
                      ),
                      SizedBox(height: 10.0),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: subText(),
                      ),
                      SizedBox(height: 10.0),
                      widget.openFrom == "APPOINTMENT_DASHBOARD"
                          ? Padding(
                              padding: const EdgeInsets.only(top: 0, bottom: 0, left: 16.0, right: 16.0),
                              child: PrimaryButton(
                                fullWidth: true,
                                text: LocalizationUtils.getSingleValueString("modal", "modal.signup.ok"),
                                onPressed: () {
                                  getNextRoute(context);
                                },
                              ),
                            )
                          : Padding(
                              padding: const EdgeInsets.only(top: 0, bottom: 0, left: 16.0, right: 16.0),
                              child: PrimaryButton(
                                fullWidth: true,
                                text: LocalizationUtils.getSingleValueString("modal", "modal.signup.complete-profile").toUpperCase(),
                                onPressed: () {
                                  getNextRoute(context);
                                },
                              ),
                            ),
                      SizedBox(height: 10.0),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  double languageFromHeight() {
    if (getSelectedLanguage() == ViewConstants.languageIdEn) {
      return 350;
    } else {
      if (getSelectedLanguage() == ViewConstants.languageIdFr && widget.openFrom == "SIGNUP_PRESCRIPTION") {
        return 330;
      } else {
        return 350;
      }
    }
  }

  Widget subText() {
    if (widget.openFrom == "SIGNUP_TELEHEALTH") {
      return Text(
        LocalizationUtils.getSingleValueString("modal", "modal.signup.telehealth-details"),
        style: TextStyle(fontSize: 16.0, color: primaryColor, fontWeight: FontWeight.normal, fontFamily: "FSJoeyPro", height: 1.5),
      );
    } else if (widget.openFrom == "DASHBOARD_TELEHEALTH") {
      return Text(
        LocalizationUtils.getSingleValueString("modal", "modal.signup.doctor-call"),
        style: TextStyle(fontSize: 16.0, color: primaryColor, fontWeight: FontWeight.normal, fontFamily: "FSJoeyPro", height: 1.5),
      );
    } else if (widget.openFrom == "SIGNUP_PRESCRIPTION") {
      return Text(
        LocalizationUtils.getSingleValueString("modal", "modal.signup.details"),
        style: TextStyle(fontSize: 16.0, color: primaryColor, fontWeight: FontWeight.normal, fontFamily: "FSJoeyPro", height: 1.5),
      );
    } else if (widget.openFrom == "APPOINTMENT_DASHBOARD") {
      return Text(
        LocalizationUtils.getSingleValueString("modal", "modal.signup.telehealth-details"),
        style: TextStyle(fontSize: 16.0, color: primaryColor, fontWeight: FontWeight.normal, fontFamily: "FSJoeyPro", height: 1.5),
      );
    } else {
      return Text("");
    }
  }

  Widget lineDivider = Divider(color: secondaryColor, height: 1);

  Widget bottomSheetItems(IconData icons, String text, String subtext) {
    return Container(
      height: 60.0,
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: EdgeInsets.all(MEDIUM_X),
        child: Row(
          children: [
            Icon(
              icons,
              color: darkBlueColor2,
            ),
            SizedBox(width: 15.0),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  text,
                  style: TextStyle(fontSize: 15.4, color: darkBlueColor2, fontWeight: FontWeight.w500, fontFamily: "FSJoeyPro Medium"),
                ),
                SizedBox(height: 1.0),
                Text(
                  subtext,
                  textAlign: TextAlign.start,
                  style: TextStyle(fontSize: 12.0, color: darkBlueColor2, fontWeight: FontWeight.w300, fontFamily: "FSJoeyPro"),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  getNextRoute(BuildContext context) {
    // await userContactModel.putContact();
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.consultation_signup_complete_profile_clicked);
    if (dataStore.readBoolean(DataStoreService.SIGNUP_TELEHEALTH) == true) {
      dataStore.writeBoolean(DataStoreService.SIGNUP_TELEHEALTH, false);
      dataStore.writeBoolean(DataStoreService.SIGNUP_TELEHEALTH, false);
      dataStore.writeBoolean(DataStoreService.DASHBOARD_TELEHEALTH, false);

      Navigator.pushNamed(context, OrderStepper.routeName, arguments: BaseStepperArguments(startStep: 1, from: 'telehealth'));
    } else if (dataStore.readBoolean(DataStoreService.DASHBOARD_TELEHEALTH) == true) {
      dataStore.writeBoolean(DataStoreService.SIGNUP_TELEHEALTH, false);
      dataStore.writeBoolean(DataStoreService.DASHBOARD_TELEHEALTH, false);
      Navigator.pop(context);
    } else if (dataStore.readBoolean(DataStoreService.SIGNUP_PRESCRIPTION) == true) {
      dataStore.writeBoolean(DataStoreService.SIGNUP_TELEHEALTH, false);
      dataStore.writeBoolean(DataStoreService.DASHBOARD_TELEHEALTH, false);
      dataStore.writeBoolean(DataStoreService.SIGNUP_PRESCRIPTION, false);
      Navigator.pushNamed(context, OrderStepper.routeName, arguments: BaseStepperArguments(startStep: 0, from: 'telehealth'));
    } else if (widget.openFrom == "APPOINTMENT_DASHBOARD") {
      Navigator.pop(context);
      dataStore.writeBoolean(DataStoreService.SIGNUP_TELEHEALTH, false);
      dataStore.writeBoolean(DataStoreService.DASHBOARD_TELEHEALTH, false);
      dataStore.writeBoolean(DataStoreService.APPOINTMENT_DASHBOARD, false);
    } else {
      Navigator.pop(context);
      dataStore.writeBoolean(DataStoreService.SIGNUP_TELEHEALTH, false);
      dataStore.writeBoolean(DataStoreService.DASHBOARD_TELEHEALTH, false);
    }
  }

  Widget getTitleView() {
    if (widget.openFrom == "SIGNUP_TELEHEALTH") {
      return Padding(
        padding: EdgeInsets.only(left: 10, right: 10),
        child: Text(
          LocalizationUtils.getSingleValueString("modal", "modal.signup.booked"),
          style: TextStyle(fontSize: 19.0, color: whiteColor, fontWeight: FontWeight.bold, fontFamily: "FSJoeyPro Bold"),
        ),
      );
    } else {
      return Padding(
        padding: EdgeInsets.only(left: 10, right: 10),
        child: Text(
          LocalizationUtils.getSingleValueString("modal", "modal.signup.congratulations") + " " + dataStore.readString(DataStoreService.FIRSTNAME)!,
          style: TextStyle(fontSize: 19.0, color: whiteColor, fontWeight: FontWeight.bold, fontFamily: "FSJoeyPro Bold"),
        ),
      );
    }
  }

  Widget getSubTitleView() {
    if (widget.openFrom == "SIGNUP_TELEHEALTH" || widget.openFrom == "APPOINTMENT_DASHBOARD") {
      return Padding(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Text(
          LocalizationUtils.getSingleValueString("modal", "modal.signup.telehealth-subtitle2")
              .replaceAll("{{doctorName}}", dataStore.readString(DataStoreService.DOCTOR_NAME)!)
              .replaceAll("{{slotDate}}", dataStore.readString(DataStoreService.APPOINTMENT_DATE)!)
              .replaceAll("{{timeSlot}}", dataStore.readString(DataStoreService.APPOINTMENT_TIME)!),
          style: TextStyle(
              fontSize: LocalizationUtils.isProvinceQuebec() ? 15.0 : 16.0, color: whiteColor, fontWeight: FontWeight.normal, fontFamily: "FSJoeyPro Medium", height: 1.5),
          textAlign: TextAlign.center,
        ),
      );
    } else {
      return Padding(
        padding: EdgeInsets.only(left: 10, right: 10),
        child: Text(
          LocalizationUtils.getSingleValueString("modal", "modal.signup.subtitle"),
          style: TextStyle(fontSize: LocalizationUtils.isProvinceQuebec() ? 15.0 : 16.0, color: whiteColor, fontWeight: FontWeight.normal, fontFamily: "FSJoeyPro Medium"),
        ),
      );
    }
  }
}
