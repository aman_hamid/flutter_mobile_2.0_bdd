import 'package:flutter/material.dart';
import 'package:pocketpills/application/pp_application.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/base/base_stepper_arguments.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/contact/prescription_source_arguments.dart';
import 'package:pocketpills/ui/views/contact/user_contact_view.dart';
import 'package:pocketpills/core/viewmodels/signup/signup_transfer_model.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/ui/views/addmember/add_member_complete.dart';
import 'package:pocketpills/ui/views/consent/consent_arguments.dart';
import 'package:pocketpills/ui/views/consent/wait_consent.dart';
import 'package:pocketpills/ui/views/contact/source_arguments.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/ui/views/home/medicine_detail_view.dart';
import 'package:pocketpills/ui/views/profile/profile_health_card_view.dart';
import 'package:pocketpills/ui/views/signup/signup_almost_done_view.dart';
import 'package:pocketpills/ui/views/signup/signup_stepper_arguments.dart';
import 'package:pocketpills/ui/views/imageupload/upload_prescription.dart';
import 'package:pocketpills/ui/views/signup/select_provins_bottom_sheet.dart';
import 'package:pocketpills/ui/views/imageupload/upload_prescription_arguments.dart';
import 'package:pocketpills/ui/views/signup/transfer_view.dart';
import 'package:pocketpills/ui/views/signup/transfer_arguments.dart';
import 'package:pocketpills/ui/views/signup/youtube_bottom_sheet.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:provider/provider.dart';

class SkipSignupSheet extends StatefulWidget {
  BuildContext? context;
  SignUpTransferModel? model;
  BaseStepperSource? source;
  UserPatient? userPatient;
  String? openFrom;

  SkipSignupSheet({this.context, this.model, this.source, this.userPatient, this.openFrom});

  _SkipSignupSheetState createState() => _SkipSignupSheetState();
}

class _SkipSignupSheetState extends BaseState<SkipSignupSheet> with SingleTickerProviderStateMixin {
  SignUpTransferModel? model;
  late BuildContext context;

  _SkipSignupSheetState();

  late SignUpTransferModel model2;

  bool doctorSelected = false, uploadSelected = false, medicationSelected = false;

  @override
  void initState() {
    super.initState();
    model = widget.model;
    model2 = SignUpTransferModel();
    context = widget.context!;
  }

  @override
  void didChangeDependencies() {
    analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.dropoff_popup_open);
  }
  // @override
  // void dispose() {
  //   super.dispose();
  // }

  Widget build(BuildContext buildContext) {
    return FutureBuilder(
        future: myFutureMethodOverall(model2),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData != null && snapshot.data != null) {
            dataStore.writeInt(DataStoreService.STEPPER_CURRENT_STEP, 0);
            return getMainView();
          } else if (snapshot.hasError) {
            return ErrorScreen();
          } else {
            return LoadingScreen();
          }
        });
  }

  Future myFutureMethodOverall(SignUpTransferModel model) async {
    Future<Map<String, dynamic>?> future1 = model.getLocalization(["signup", "dropoff"]);
    return await Future.wait([future1]);
  }

  Widget getMainView() {
    return Container(
      height: languageFrom(),
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Container(
            decoration: new BoxDecoration(
                color: Colors.white, //new Color.fromRGBO(255, 0, 0, 0.0),
                borderRadius: new BorderRadius.only(topLeft: const Radius.circular(10.0), topRight: const Radius.circular(10.0))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  height: 102.0,
                  alignment: Alignment.bottomCenter,
                  decoration: new BoxDecoration(
                      color: brandColor, //new Color.fromRGBO(255, 0, 0, 0.0),
                      borderRadius: new BorderRadius.only(topLeft: const Radius.circular(10.0), topRight: const Radius.circular(10.0))),
                  child: Padding(
                    padding: EdgeInsets.only(bottom: MEDIUM_XX),
                    child: Text(
                      LocalizationUtils.getSingleValueString("signup", "signup.dropoff.description"),
                      style: TextStyle(fontSize: 18.0, color: whiteColor, fontWeight: FontWeight.w500, fontFamily: "FSJoeyPro Bold"),
                    ),
                  ),
                ),
                widget.openFrom != "preference_sheet"
                    ? InkWell(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.pushNamed(context, UploadPrescription.routeName,
                              arguments: UploadPrescriptionArguments(
                                from: "telehealth",
                                source: widget.source,
                                model: model,
                                userPatient: widget.userPatient,
                              ));
                        },
                        child: bottomSheetItems(
                          Icons.post_add,
                          LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.transfer-upload-title"),
                          LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.transfer-upload-description"),
                        ),
                      )
                    : Container(),
                lineDivider,
                (widget.openFrom != "preference_sheet" && !LocalizationUtils.isProvinceQuebec())
                    ? InkWell(
                        onTap: () {
                          if (doctorSelected == false) {
                            doctorSelected = true;
                            analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.dropoff_popup_telehealth);
                          }
                          Navigator.pop(context);
                          model!.setProvincePop("clicked");
                          showCallAndChatBottomSheet(context, model!);
                        },
                        child: bottomSheetItems(
                          Icons.record_voice_over_outlined,
                          LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.transfer-telehealth-title"),
                          LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.transfer-telehealth-description"),
                        ),
                      )
                    : Container(),
                !LocalizationUtils.isProvinceQuebec() ? lineDivider : Container(),
                (widget.openFrom != "preference_sheet" && !LocalizationUtils.isProvinceQuebec())
                    ? InkWell(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.pushNamed(context, MedicineDetailWidget.routeName,
                              arguments: UploadPrescriptionArguments(
                                from: "telehealth",
                                source: widget.source,
                                model: model,
                                userPatient: widget.userPatient,
                              ));
                        },
                        child: bottomSheetItems(
                          Icons.search,
                          LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.transfer-search-title"),
                          LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.transfer-search-description"),
                        ),
                      )
                    : Container(),
                !LocalizationUtils.isProvinceQuebec() ? lineDivider : Container(),
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                    openVideoPlayer();
                  },
                  child: bottomSheetItems(
                    Icons.play_circle_outline_outlined,
                    LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.consultation-video-title"),
                    LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.consultation-video-description"),
                  ),
                ),
                widget.openFrom == "preference_sheet"
                    ? InkWell(
                        onTap: () {
                          Navigator.pop(context);
                          // Navigator.of(context).pushNamedAndRemoveUntil(TransferWidget.routeName);
                          Navigator.of(context)
                              .pushNamedAndRemoveUntil(TransferWidget.routeName, (Route<dynamic> route) => false, arguments: TransferArguments(source: BaseStepperSource.NEW_USER));
                        },
                        child: bottomSheetItems(
                          Icons.post_add_outlined,
                          LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.consultation-transfer-title"),
                          LocalizationUtils.getSingleValueString("dropoff", "dropoff.options.consultation-transfer-description"),
                        ),
                      )
                    : Container(),
                SizedBox(height: 20.0),
                widget.openFrom != "preference_sheet"
                    ? Padding(
                        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                        child: RaisedButton(
                          child: Text(
                            LocalizationUtils.getSingleValueString("dropoff", "dropoff.all.transfer-skip").toUpperCase(),
                            style: TextStyle(color: Colors.grey, fontFamily: "FSJoeyPro Bold", fontSize: 16.0),
                          ),
                          color: Colors.white,
                          shape: RoundedRectangleBorder(
                            side: BorderSide(color: tertiaryColor),
                            borderRadius: BorderRadius.circular(SMALL_XX),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                            onSkipClick(model!);
                            //goToNextScreen(model);
                          },
                        ),
                      )
                    : Padding(
                        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                        child: RaisedButton(
                          child: Text(
                            LocalizationUtils.getSingleValueString("common", "common.button.close"),
                            style: TextStyle(color: Colors.grey, fontFamily: "FSJoeyPro Bold"),
                          ),
                          color: Colors.white,
                          shape: RoundedRectangleBorder(
                            side: BorderSide(color: tertiaryColor),
                            borderRadius: BorderRadius.circular(SMALL_XX),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                            //goToNextScreen(model);
                          },
                        ),
                      ),
                SizedBox(height: 10.0),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 10),
            child: Align(
              alignment: topAlignment()!,
              child: CircleAvatar(
                  radius: 40,
                  backgroundColor: lightBlueColor,
                  child: CircleAvatar(
                    radius: 36,
                    backgroundColor: brandColor,
                    backgroundImage: CachedNetworkImageProvider(
                      "https://static.pocketpills.com/dashboard/pharmacist/cathy.jpg",
                    ),
                  )),
            ),
          ),
        ],
      ),
    );
  }

  openVideoPlayer() {
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: false,
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return YouTubeSheet(
            context: context,
          );
        });
  }

  double? languageFrom() {
    if (getSelectedLanguage() == ViewConstants.languageIdEn && widget.openFrom == "preference_sheet") {
      return 310;
    } else if (getSelectedLanguage() == ViewConstants.languageIdEn && widget.openFrom != "preference_sheet") {
      return 444;
    } else if (getSelectedLanguage() == ViewConstants.languageIdFr && widget.openFrom == "preference_sheet") {
      return 310;
    } else if (getSelectedLanguage() == ViewConstants.languageIdFr && widget.openFrom != "preference_sheet") {
      return 470;
    }
  }

  AlignmentGeometry? topAlignment() {
    if (widget.openFrom == "transfer" && LocalizationUtils.isProvinceQuebec()) {
      return Alignment(0.0, -.34);
    } else if (widget.openFrom == "preference_sheet") {
      return Alignment(0.0, -1.3);
    } else if (widget.openFrom == "transfer") {
      return Alignment(0.0, -1);
    }
  }

  onSkipClick(SignUpTransferModel model) async {
    if (widget.source == BaseStepperSource.NEW_USER || widget.source == BaseStepperSource.ADD_PATIENT) {
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_transfer_skipped);
      //bool success = await model.uploadPrescription(widget.userPatient, null, null, null, null, false, null, true, "PENDING", null, null, null);
      bool success = await model.uploadPrescriptionSignUp();
      print("here");
      if (success == true) {
        print("success");
        dataStore.writeBoolean(DataStoreService.TRANSFER_SKIPPED, true);
        goToNextScreen(model);
      }
    } else
      Navigator.pop(context);
  }

  void goToNextScreen(SignUpTransferModel signUpTransferModel) {
    if (widget.source == BaseStepperSource.NEW_USER) {
      dataStore.writeBoolean(DataStoreService.COMPLETE_TRANSFER_MODULE, true);
      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.account_contact_detail);
      Navigator.of(context)
          .pushNamedAndRemoveUntil(SignUpAlmostDoneWidget.routeName, (Route<dynamic> route) => false, arguments: SignupStepperArguments(source: BaseStepperSource.NEW_USER));
    } else if (widget.source == BaseStepperSource.ADD_PATIENT) {
      Navigator.pushReplacementNamed(context, AddMemberCompleteWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.CONSENT_FLOW) {
      Navigator.pushReplacementNamed(context, WaitConsentWidget.routeName, arguments: ConsentArguments(userPatient: widget.userPatient));
    } else if (widget.source == BaseStepperSource.MAIN_SCREEN) {
      Navigator.of(context).pushReplacementNamed(UserContactWidget.routeName,
          arguments: PrescriptionSourceArguments(source: BaseStepperSource.TRANSFER_SCREEN, successDetails: signUpTransferModel.successDetails));
    } else if (widget.source == BaseStepperSource.COPAY_REQUEST) {
      Navigator.of(context).pushNamed(ProfileHealthCardView.routeName, arguments: SourceArguments(source: widget.source));
    }
  }

  showCallAndChatBottomSheet(BuildContext context, SignUpTransferModel model) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SMALL_XXX),
        ),
        builder: (BuildContext bottomSheetContext) {
          return SelectProvinsSheet(context: context, model: model, source: widget.source, userPatient: widget.userPatient);
        });
  }

  Widget lineDivider = Divider(color: secondaryColor, height: 1);

  Widget bottomSheetItems(IconData icons, String text, String subtext) {
    return Wrap(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: EdgeInsets.all(MEDIUM_X),
            child: Row(
              children: [
                Icon(
                  icons,
                  color: darkBlueColor2,
                ),
                SizedBox(width: 15.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 350,
                      child: Text(
                        text,
                        style: TextStyle(fontSize: 16.0, color: darkBlueColor2, fontWeight: FontWeight.w500, fontFamily: "FSJoeyPro Bold"),
                      ),
                    ),
                    SizedBox(height: 1.0),
                    Wrap(
                      children: [
                        Container(
                          width: 300,
                          child: Text(
                            subtext,
                            textAlign: TextAlign.start,
                            style: TextStyle(fontSize: 14.4, color: darkBlueColor2, fontWeight: FontWeight.w300, fontFamily: "FSJoeyPro"),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
