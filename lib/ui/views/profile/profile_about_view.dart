import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pocketpills/core/enums/viewstate.dart';
import 'package:pocketpills/core/models/contact.dart';
import 'package:pocketpills/core/models/user_patient.dart';
import 'package:pocketpills/core/response/signup/employer_suggestion_response.dart';
import 'package:pocketpills/core/services/shared_prefs.dart';
import 'package:pocketpills/core/viewmodels/dashboard_model.dart';
import 'package:pocketpills/core/viewmodels/home_model.dart';
import 'package:pocketpills/core/viewmodels/profile/profile_about_model.dart';
import 'package:pocketpills/core/viewmodels/signup/about_you_model.dart';
import 'package:pocketpills/core/viewmodels/user_contact_model.dart';
import 'package:pocketpills/res/colors.dart';
import 'package:pocketpills/ui/base/scaffold_base.dart';
import 'package:pocketpills/ui/shared/buttons/primary_button.dart';
import 'package:pocketpills/ui/shared/constants/localized_data.dart';
import 'package:pocketpills/ui/shared/constants/view_constants.dart';
import 'package:pocketpills/ui/shared/pp_bottombars.dart';
import 'package:pocketpills/ui/shared/pp_container.dart';
import 'package:pocketpills/ui/shared/pp_formfields.dart';
import 'package:pocketpills/ui/shared/pp_inputdecor.dart';
import 'package:pocketpills/ui/shared/pp_texts.dart';
import 'package:pocketpills/ui/shared/pp_uihelper.dart';
import 'package:pocketpills/res/size_style.dart';
import 'package:pocketpills/ui/shared/staticscreens/loading_screen.dart';
import 'package:pocketpills/ui/views/base_state.dart';
import 'package:pocketpills/ui/views/error_screen.dart';
import 'package:pocketpills/utils/localization/localization_utils.dart';
import 'package:pocketpills/utils/string_utils.dart';
import 'package:provider/provider.dart';

class ProfileAboutView extends StatefulWidget {
  ProfileAboutView({Key? key}) : super(key: key);

  @override
  _ProfileAboutViewState createState() => _ProfileAboutViewState();
}

class _ProfileAboutViewState extends BaseState<ProfileAboutView> {
  final TextEditingController _invitationCodeController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();

  String? dropDownValue;
  String? languageDropDownValue;
  String? dropDownError;
  int? patient_id;

  bool email = false;
  bool autovalidate = false;

  @override
  void initState() {
    super.initState();
    _emailController.addListener(emailListener);
  }

  emailListener() {
    if (email == false) {
      email = true;
    }
  }

  @override
  void dispose() {
    super.dispose();
    _emailController.removeListener(emailListener);
  }

  Widget getBottomButton(BuildContext context, ProfileAboutModel profileAboutModel, UserContactModel userContactModel) {
    UserPatient? curPatient = Provider.of<DashboardModel>(context).selectedPatient;
    patient_id = curPatient != null ? curPatient.patientId : null;
    if ((curPatient != null &&
        curPatient.patient!.insuranceDetailsSet != null &&
        curPatient.patient!.insuranceDetailsSet!.length > 0 &&
        curPatient.patient!.employerDisplayName != null)) {
      return SizedBox(
        height: 0.0,
      );
    } else {
      return PPBottomBars.getButtonedBottomBar(
          child: PrimaryButton(
        text: LocalizationUtils.getSingleValueString("profile", "profile.button.update"),
        fullWidth: true,
        onPressed: onUpdateClick(context, profileAboutModel, userContactModel),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ProfileAboutModel>(create: (_) => ProfileAboutModel()),
        ChangeNotifierProvider<UserContactModel>(create: (_) => UserContactModel()),
        ChangeNotifierProvider<HomeModel>(create: (_) => HomeModel()),
        ChangeNotifierProvider<DashboardModel>(create: (_) => DashboardModel()),
      ],
      child: Consumer3<ProfileAboutModel, UserContactModel, DashboardModel>(builder:
          (BuildContext context, ProfileAboutModel profileAboutModel, UserContactModel userContactModel, DashboardModel dashboardModel, Widget? child) {
        return FutureBuilder(
            future: profileAboutModel.getLocalization(["profile"]),
            builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>?> snapshot) {
              if (snapshot.hasData != null && snapshot.data != null) {
                return getMainView(profileAboutModel, userContactModel, context);
              } else if (snapshot.hasError) {
                return ErrorScreen();
              } else {
                return LoadingScreen();
              }
            });
      }),
    );
  }

  Widget showEmailField(BuildContext context) {
    UserPatient? selectedPatient = Provider.of<DashboardModel>(context).selectedPatient;
    if (_emailController.text.isEmpty) {
      _emailController.text = selectedPatient!.patient!.email == null ? "" : selectedPatient.patient!.email!;
    }

    return PPFormFields.getTextField(
        autovalidate: autovalidate,
        enabled: selectedPatient!.patient!.email != null ? false : true,
        keyboardType: TextInputType.emailAddress,
        controller: _emailController,
        textInputAction: TextInputAction.done,
        onErrorStr: LocalizationUtils.getSingleValueString("common", "common.label.required") + "*",
        decoration: PPInputDecor.getDecoration(labelText: 'E-mail Address', hintText: 'Enter Your Email', labelColor: primaryColor).copyWith(
          floatingLabelBehavior: FloatingLabelBehavior.always,
          errorText: LocalizationUtils.getSingleValueString("common", "common.label.required") + "*",
        ),
        onFieldSubmitted: (value) {});
  }

  Widget getMainView(ProfileAboutModel profileAboutModel, UserContactModel userContactModel, BuildContext context) {
    return BaseScaffold(
        bottomNavigationBar: Builder(
            builder: (BuildContext context) =>
                profileAboutModel.state == ViewState.Busy ? ViewConstants.progressIndicator : getBottomButton(context, profileAboutModel, userContactModel)),
        body: Container(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: PPUIHelper.HorizontalSpaceMedium, right: PPUIHelper.HorizontalSpaceMedium, top: MEDIUM),
              child: Column(
                children: <Widget>[
                  PPUIHelper.verticalSpaceLarge(),
                  PPFormFields.getTextField(
                    controller: TextEditingController(text: Provider.of<DashboardModel>(context).selectedPatient?.patient!.firstName ?? "-"),
                    enabled: false,
                    textCapitalization: TextCapitalization.words,
                    decoration: PPInputDecor.getDecoration(
                        labelText: LocalizationUtils.getSingleValueString("profile", "profile.labels.firstname"),
                        hintText: LocalizationUtils.getSingleValueString("profile", "profile.labels.firstname"),
                        labelColor: primaryColor),
                  ),
                  PPUIHelper.verticalSpaceMedium(),
                  PPFormFields.getTextField(
                    enabled: false,
                    controller: TextEditingController(text: Provider.of<DashboardModel>(context).selectedPatient?.patient!.lastName ?? "-"),
                    textCapitalization: TextCapitalization.words,
                    decoration: PPInputDecor.getDecoration(
                        labelText: LocalizationUtils.getSingleValueString("profile", "profile.labels.lastname"),
                        hintText: LocalizationUtils.getSingleValueString("profile", "profile.labels.lastname"),
                        labelColor: primaryColor),
                  ),
                  Provider.of<DashboardModel>(context).selectedPatient!.patient!.phone == null
                      ? Container()
                      : Container(
                          child: Column(
                            children: <Widget>[
                              PPUIHelper.verticalSpaceMedium(),
                              PPFormFields.getTextField(
                                enabled: false,
                                controller: TextEditingController(text: Provider.of<DashboardModel>(context).selectedPatient!.patient!.phone.toString()),
                                textCapitalization: TextCapitalization.words,
                                decoration: PPInputDecor.getDecoration(
                                    labelText: LocalizationUtils.getSingleValueString("profile", "profile.labels.phone"),
                                    hintText: LocalizationUtils.getSingleValueString("profile", "profile.labels.phone"),
                                    labelColor: primaryColor),
                              ),
                            ],
                          ),
                        ),
                  PPUIHelper.verticalSpaceMedium(),
                  PPFormFields.getTextField(
                    enabled: false,
                    controller: TextEditingController(text: Provider.of<DashboardModel>(context).selectedPatient!.patient!.birthDate ?? "-"),
                    textCapitalization: TextCapitalization.words,
                    decoration: PPInputDecor.getDecoration(
                        labelText: LocalizationUtils.getSingleValueString("profile", "profile.labels.dob"),
                        hintText: LocalizationUtils.getSingleValueString("profile", "profile.labels.dob"),
                        labelColor: primaryColor),
                  ),
                  PPUIHelper.verticalSpaceMedium(),
                  PPFormFields.getTextField(
                    enabled: false,
                    controller: TextEditingController(text: Provider.of<DashboardModel>(context).selectedPatient!.patient!.gender ?? "-"),
                    textCapitalization: TextCapitalization.words,
                    decoration: PPInputDecor.getDecoration(
                        labelText: LocalizationUtils.getSingleValueString("profile", "profile.labels.gender"),
                        hintText: LocalizationUtils.getSingleValueString("profile", "profile.labels.gender"),
                        labelColor: primaryColor),
                  ),

                  PPUIHelper.verticalSpaceMedium(),

                  showEmailField(context),

                  //getInvitationCode(context, dashboardModel, profileAboutModel),
                  _getLanguagePreference(profileAboutModel),
                  PPUIHelper.verticalSpaceMedium(),
                  FutureBuilder(
                    future: userContactModel.fetchContactDetails(),
                    builder: (BuildContext context, AsyncSnapshot<Contact?> snapshot) {
                      return Container(
                        child: Builder(
                          builder: (BuildContext context) {
                            if (snapshot.hasData != null || snapshot.data == null) {
                              return _afterFutureResolved(context, userContactModel);
                            } else {
                              return PPContainer.emptyContainer();
                            }
                          },
                        ),
                      );
                    },
                  ),
                  PPUIHelper.verticalSpaceMedium(),
                ],
              ),
            ),
          ),
        ));
  }

  Widget _getLanguagePreference(ProfileAboutModel profileAboutModel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        PPUIHelper.verticalSpaceMedium(),
        PPTexts.getFormLabel(
          LocalizationUtils.getSingleValueString("profile", "profile.labels.language"),
          isBold: true,
        ),
        PPUIHelper.verticalSpaceSmall(),
        PPFormFields.getDropDown(
          value: languageDropDownValue == null
              ? profileAboutModel.getStoredLanguage() == "en"
                  ? "en"
                  : "fr"
              : languageDropDownValue!,
          onChanged: (String? newValue) {
            languageDropDownValue = newValue;
            profileAboutModel.setLanguageDropDown(newValue!);
          },
          items: ViewConstants.languageMap
              .map((String key, String value) {
                return MapEntry(
                  key,
                  DropdownMenuItem<String>(
                    value: key,
                    child: Text(value),
                  ),
                );
              })
              .values
              .toList(),
        ),
      ],
    );
  }

  Widget _afterFutureResolved(BuildContext context, UserContactModel userContactModel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            PPTexts.getFormLabel(LocalizationUtils.getSingleValueString("profile", "profile.timings.title")),
            PPUIHelper.verticalSpaceMedium(),
            PPTexts.getFormLabel(LocalizationUtils.getSingleValueString("modals", "modals.timings.day")),
            PPUIHelper.verticalSpaceSmall(),
            PPFormFields.getDropDown(
              value: userContactModel.day,
              onChanged: (String? newValue) async {
                userContactModel.setDay(newValue!);
                bool result = await userContactModel.putContact();
                if (result == true) {
                  onFail(context, errMessage: userContactModel.errorMessage);
                }
                analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.profile_user_contact_day_change);
              },
              items: getDayMap()
                  .map((String key, String value) {
                    return MapEntry(
                        key,
                        DropdownMenuItem<String>(
                          value: key,
                          child: Text(value),
                        ));
                  })
                  .values
                  .toList(),
            ),
            PPUIHelper.verticalSpaceMedium(),
            PPTexts.getFormLabel(LocalizationUtils.getSingleValueString("modals", "modals.timings.time")),
            PPUIHelper.verticalSpaceSmall(),
            PPFormFields.getDropDown(
              value: userContactModel.time,
              onChanged: (String? newValue) async {
                userContactModel.setTime(newValue!);
                bool result = await userContactModel.putContact();
                if (result == true) {
                  onFail(context, errMessage: userContactModel.errorMessage);
                }
                analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.profile_user_contact_time_change);
              },
              items: getTimeMap()
                  .map((String key, String value) {
                    return MapEntry(
                        key,
                        DropdownMenuItem<String>(
                          value: key,
                          child: Text(value),
                        ));
                  })
                  .values
                  .toList(),
            ),
          ],
        )),
        SizedBox(
          height: LARGE_X,
        )
      ],
    );
  }

  Widget getInvitationCode(BuildContext context, DashboardModel dashboardModel, ProfileAboutModel profileAboutModel) {
    UserPatient? curPatient = Provider.of<DashboardModel>(context).selectedPatient;
    if (curPatient!.primary != true) return Container();
    if (curPatient.patient!.insuranceDetailsSet != null &&
        curPatient.patient!.insuranceDetailsSet!.length > 0 &&
        curPatient.patient!.employerDisplayName != null)
      return Container(
          child: Column(
        children: <Widget>[
          PPFormFields.getTextField(
            enabled: false,
            controller: TextEditingController(text: curPatient.patient!.employerDisplayName),
            textCapitalization: TextCapitalization.characters,
            decoration: PPInputDecor.getDecoration(
              labelText: 'Employer Code',
            ),
          ),
        ],
      ));
    else
      return FutureBuilder(
        future: profileAboutModel.getSuggestedEmployers(),
        builder: (BuildContext context, AsyncSnapshot<EmployerSuggestionResponse?> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data != null && snapshot.data!.suggestionsValidCount! > 0) {
              return afterFutureResolved(context, profileAboutModel, snapshot.data!);
            } else {
              return Container(
                child: Column(
                  children: <Widget>[
                    PPFormFields.getTextField(
                        controller: _invitationCodeController,
                        decoration: PPInputDecor.getDecoration(labelText: 'Employer Code (Optional)', hintText: 'Enter employer code here'),
                        validator: (value) {
                          return null;
                        }),
                    PPUIHelper.verticalSpaceMedium(),
                  ],
                ),
              );
            }
          } else if (snapshot.hasError) {
            FirebaseCrashlytics.instance.log(snapshot.hasError.toString());
            return PPContainer.emptyContainer();
          }
          return PPContainer.emptyContainer();
        },
      );
  }

  onUpdateClick(BuildContext context, ProfileAboutModel profileAboutModel, UserContactModel userContactModel) {
    return () async {
      bool connectivityResult = await profileAboutModel.isInternetConnected();

      if (connectivityResult == false) {
        onFail(context, errMessage: profileAboutModel.noInternetConnection);
        return;
      }

      if ((_emailController.text != null && _emailController.text == '') && !StringUtils.isEmail(_emailController.text)) {
        onFail(context, errMessage: 'Invalid Email');
        return;
      }

      analyticsEvents.sendAnalyticsEvent(AnalyticsEventConstant.click_profile_employer_updated);
      if ((_invitationCodeController.text != null && _invitationCodeController.text.length > 0)) {
        var inviteRes = await profileAboutModel.updateInvitationCode(_invitationCodeController.text);
        showOnSnackBar(context, successMessage: inviteRes);
        Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
        await Provider.of<DashboardModel>(context, listen: false).getAndSetUserPatientList();
      }

      if (languageDropDownValue != null) {
        bool? success = await profileAboutModel.updateLanguage(languageDropDownValue!);
        if (success!) {
          profileAboutModel.setLanguage(context, languageDropDownValue!);
        }
      }

      String email = _emailController.text;
      if (await profileAboutModel.updateMemberEmail(patient_id!, email)) {
        await Provider.of<DashboardModel>(context, listen: false).setEmail(email);
        await Provider.of<DashboardModel>(context, listen: false).clearUserPatientList();
        await Provider.of<DashboardModel>(context, listen: false).getAndSetUserPatientList();
      }
    };
  }

  Widget afterFutureResolved(BuildContext context, ProfileAboutModel profileAboutModel, EmployerSuggestionResponse data) {
    _emailController.text = profileAboutModel.email;
    return PPFormFields.getDropDown(
      labelText: "Select Employer",
      fullWidth: true,
      value: dropDownValue!,
      isError: false,
      items: data.suggestedEmployers!
          .map((state) => DropdownMenuItem<String>(
              value: state.employerName! +
                  " " +
                  ((state.invitationCodeDataSet != null && state.invitationCodeDataSet!.length > 0) ? state.invitationCodeDataSet![0].invitationCode! : ""),
              child: Text(state.employerDisplayName!)))
          .toList(),
      onChanged: (selectedItem) => setState(() {
        dropDownValue = selectedItem;
        if (selectedItem!.split(" ")[1] != null) {
          _invitationCodeController.text = selectedItem.split(" ")[1];
        } else {
          _invitationCodeController.text = selectedItem.split(" ")[0];
        }
        setState(() {
          dropDownError = null;
        });
      }),
    );
  }
}

String getGender(String? gender) {
  if (gender!.toLowerCase() == "male") {
    return LocalizationUtils.getSingleValueString("common", "common.all.gender-male");
  } else if (gender.toLowerCase() == "female") {
    return LocalizationUtils.getSingleValueString("common", "common.all.gender-female");
  } else {
    return LocalizationUtils.getSingleValueString("common", "common.all.gender-other");
  }
}
