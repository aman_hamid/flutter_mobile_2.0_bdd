import 'package:pocketpills/core/models/user_patient.dart';

class ConsentArguments {
  final UserPatient? userPatient;
  final String? from;

  ConsentArguments({this.userPatient, this.from});
}
