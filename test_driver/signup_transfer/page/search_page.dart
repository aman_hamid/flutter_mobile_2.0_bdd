import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';

class SearchProcess {
  final pharmacyView = find.byValueKey('PharmacySearchView');
  final searchTextView = find.byValueKey('searchTextView');

  FlutterDriver? _driver;

  //Constructor
  SearchProcess(FlutterDriver? driver) {
    this._driver = driver;
  }

  Future<bool> getSearchView() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, pharmacyView);
    return value;
  }

  Future<bool> getSearchTextView() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, searchTextView);
    return value;
  }
}
