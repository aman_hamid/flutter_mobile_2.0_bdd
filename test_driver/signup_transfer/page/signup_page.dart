import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';

class SignupProcess {
  final txtValue = find.byValueKey('phoneField');
  final firstName = find.byValueKey('firstName');
  final signupView = find.byValueKey('SignupView');
  final lastName = find.byValueKey('lastName');
  final dobMM = find.byValueKey('dobMM');
  final dobDD = find.byValueKey('dobDD');
  final dobYYYY = find.byValueKey('dobYYYY');
  final btnProceed = find.byValueKey('buttonProceed');
  final btnContinue = find.byValueKey('buttonContinue');

  FlutterDriver? _driver;

  //Constructor
  SignupProcess(FlutterDriver? driver) {
    this._driver = driver;
  }

  Future<bool> getTextField() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, txtValue);
    return value;
  }

  Future<bool> getButtonProceed() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, btnProceed);
    return value;
  }

  Future<bool> getSignupView() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, signupView);
    print(value);
    return value;
  }

  Future<bool> getFirstName() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, firstName);
    print(value);
    return value;
  }

  Future<bool> getLastName() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, lastName);
    print(value);
    return value;
  }

  Future<bool> getDobMM() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, dobMM);
    print(value);
    return value;
  }

  Future<bool> getDobDD() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, dobDD);
    print(value);
    return value;
  }

  Future<bool> getDobYYYY() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, dobYYYY);
    print(value);
    return value;
  }

  Future<bool> getContinueButton() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, btnContinue);
    print(value);
    return value;
  }
}
