import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';

class AlmostDoneProcess {
  final almostDoneView = find.byValueKey('AlmostDoneView');
  final eddDropDown = find.byValueKey('eddDropDown');
  final provinceDropDown = find.byValueKey('provinceDropDown');
  final addEmail = find.byValueKey('addEmail');
  final addPhone = find.byValueKey('addPhone');
  final selfMedicationRadioGroup = find.byValueKey('selfMedicationRadioGroup');
  final lovedOneRadioGroup = find.byValueKey('lovedOneRadioGroup');
  final genderRadioGroup = find.byValueKey('genderRadioGroup');
  final pharmacyVisible = find.byValueKey('pharmacyVisible');
  final changePharmacy = find.byValueKey('changePharmacy');
  final buttonNext = find.byValueKey('buttonNext');

  FlutterDriver? _driver;

  //Constructor
  AlmostDoneProcess(FlutterDriver? driver) {
    this._driver = driver;
  }

  Future<bool> getAlmostView() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, almostDoneView);
    return value;
  }

  Future<bool> getEddDropDown() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, eddDropDown);
    return value;
  }

  Future<bool> getProvinceDropDown() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, provinceDropDown);
    return value;
  }

  Future<bool> getButtonNext() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, buttonNext);
    return value;
  }

  Future<void> tapOnNextButton() async {
    await FlutterDriverUtils.tap(_driver!, buttonNext);
  }

  Future<void> tapOnDrpDown() async {
    await FlutterDriverUtils.tap(_driver!, eddDropDown);
  }

  Future<void> tapOnProvinceDrpDown() async {
    await FlutterDriverUtils.tap(_driver!, provinceDropDown);
  }

  Future<bool> getAddEmail() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, addEmail);
    return value;
  }

  Future<bool> getAddPhone() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, addPhone);
    return value;
  }

  Future<bool> getMedicationRadio() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, selfMedicationRadioGroup);
    return value;
  }

  Future<bool> getLovedOneRadio() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, lovedOneRadioGroup);
    return value;
  }

  Future<bool> getGenderRadio() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, genderRadioGroup);
    return value;
  }

  Future<bool> getPharmacyView() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, pharmacyVisible);
    return value;
  }

  Future<bool> getChangePharmacy() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, changePharmacy);
    return value;
  }
}
