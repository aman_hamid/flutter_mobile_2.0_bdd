import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';

class TransferProcess {
  final transferView = find.byValueKey('TransferView');
  final skipButton = find.byValueKey('SkipText');
  final iWillDoLater = find.byValueKey('IWillDoLater');
  final searchClick = find.byValueKey('searchClick');
  final buttonTransfer = find.byValueKey('buttonTransfer');

  FlutterDriver? _driver;

  //Constructor
  TransferProcess(FlutterDriver? driver) {
    this._driver = driver;
  }

  Future<bool> getTransfer() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, transferView);
    return value;
  }

  Future<bool> getSkipButton() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, skipButton);
    return value;
  }

  Future<void> getIWillDoLater() async {
    await FlutterDriverUtils.isPresent(_driver!, iWillDoLater);
  }

  Future<bool> getIWillDoLate() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, iWillDoLater);
    return value;
  }

  Future<bool> getSearchClick() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, searchClick);
    return value;
  }

  Future<bool> getButtonTransfer() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, buttonTransfer);
    return value;
  }
}
