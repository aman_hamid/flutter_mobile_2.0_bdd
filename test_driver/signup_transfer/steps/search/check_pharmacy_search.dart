import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../../page/search_page.dart';
import '../../page/transfer_page.dart';

class CheckPharmacySearchViewPresent extends GivenWithWorld<FlutterWorld> {
  @override
  Future<void> executeStep() async {
    SearchProcess process = SearchProcess(world.driver);
    bool isSearchPresent = await process.getSearchView();
    bool isSearchTextPresent = await process.getSearchTextView();

    expectMatch(isSearchPresent, true);
    expectMatch(isSearchTextPresent, false);
  }

  @override
  RegExp get pattern => RegExp(r"I have Search view and all other widgets on screen");
}
