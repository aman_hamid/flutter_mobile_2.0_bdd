import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../../page/transfer_page.dart';

class CheckTransferViewPresent extends GivenWithWorld<FlutterWorld> {
  @override
  Future<void> executeStep() async {
    TransferProcess process = TransferProcess(world.driver);
    bool isTransferPresent = await process.getTransfer();
    bool isIWillDoPresent = await process.getIWillDoLate();
    bool isSearchClick = await process.getSearchClick();
    bool isButtonTransfer = await process.getButtonTransfer();

    expectMatch(isTransferPresent, true);
    if (isIWillDoPresent) {
      expectMatch(isIWillDoPresent, true);
    } else {
      expectMatch(isIWillDoPresent, false);
    }
    expectMatch(isSearchClick, true);
    expectMatch(isButtonTransfer, true);
  }

  @override
  RegExp get pattern => RegExp(r"I have Transfer view and all other widgets on screen");
}
