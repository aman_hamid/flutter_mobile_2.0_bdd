import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../../page/otp_verify_page.dart';

class ClickViewDismiss extends AndWithWorld<FlutterWorld> {
  @override
  Future<void> executeStep() async {
    VerificationProcess process = VerificationProcess(world.driver);
    await process.getViewDismiss();
  }

  @override
  RegExp get pattern => RegExp(r"I tap on the close button");
}
