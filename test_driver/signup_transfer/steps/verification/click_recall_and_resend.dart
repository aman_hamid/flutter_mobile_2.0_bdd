import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../../page/almost_done_page.dart';
import '../../page/otp_verify_page.dart';

class ClickResendRecall extends ThenWithWorld<FlutterWorld> {
  @override
  Future<void> executeStep() async {
    VerificationProcess process = VerificationProcess(world.driver);
    await process.tapOnResendRecallButton();
  }

  @override
  RegExp get pattern => RegExp(r"I tap on the resend or call button");
}
