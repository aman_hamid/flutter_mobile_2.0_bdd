Feature: Main Screen Validates and then Logins are ok

    Scenario: Validates the phone field and login page is visible
        Then I pause for 5 seconds
        When I fill the "phoneField" field with "1111111111"
        Then I tap the "buttonProceed" button
        Then I pause for 3 seconds
        Given I have "LoginPage" on screen
        Then I pause for 3 seconds
        When I fill the "phoneFieldLogin" field with "1111111111"
        When I fill the "loginPasswordField" field with "123456789"
        Then I tap the "loginProceed" button
        Then I pause for 5 seconds
        Given I have "DashBoard" on screen
        Then I pause for 3 seconds
        Given Then I tap the "square VITAMINS_LANDING" button
        Given I have "talkToExpert" on screen
        Given I have "createPack" on screen
        Then I tap the "talkToExpert" button
        Given I have "talkExpertBottom" on screen

    Scenario: Validates the phone field and login page is visible


        Then I pause for 3 seconds
        Given Then I tap the "square VITAMINS_LANDING" button
        Given I have "talkToExpert" on screen
        Given I have "createPack" on screen
        Then I tap the "createPack" button
        Then I pause for 3 seconds
        Given I have "vitaminList" on screen
        Then I tap the "vitaminItemClick1" button
        Then I pause for 3 seconds
        Then I tap the "vitaminFilterClick" button
        Given I have "vitaminCatalog" on screen
        Then I pause for 3 seconds
        Then I tap the "vitaminDetails1" button
        Then I pause for 3 seconds
        Then I tap the "detailBack" button

    Scenario: Validates the phone field and login page is visible
        Then I pause for 3 seconds
        Given Then I tap the "square VITAMINS_LANDING" button
        Given I have "talkToExpert" on screen
        Given I have "createPack" on screen
        Then I tap the "createPack" button
        Then I pause for 3 seconds
        Given I have "vitaminList" on screen
        Then I tap the "vitaminItemClick1" button
        Then I pause for 3 seconds
        Then I tap the "vitaminFilterClick" button
        Given I have "vitaminCatalog" on screen
        Then I pause for 3 seconds
        Then I tap the "vitaminAddRemove1" button
        Then I tap the "vitaminNext" button
        Then I pause for 3 seconds
#        Given I have "vitaminListReview" on screen
#        Then I tap the "vitaminRemoveReview0" button
#        Then I pause for 4 seconds
        Then I tap the "vitaminEditDoseReview0" button
        Then I tap the "vitaminTiming2" button
        Then I tap the "vitaminTimingSave" button
        Then I pause for 3 seconds
        Then I tap the "vitaminNext" button
        Then I pause for 5 seconds
        Then I tap the "vitaminNext" button
        Then I pause for 3 seconds
        Then I tap the "vitaminBack" button
        Then I pause for 3 seconds
        Then I tap the "vitaminAddressDelete0" button