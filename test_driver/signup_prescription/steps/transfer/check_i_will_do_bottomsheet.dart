import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../../page/transfer_page.dart';

class CheckBottomSheetPresent extends GivenWithWorld<FlutterWorld> {
  @override
  Future<void> executeStep() async {
    TransferProcess process = TransferProcess(world.driver);
    bool isBottomSheetPresent = await process.getBottomSheet();
    bool isPrescriptionButtonPresent = await process.getPrescriptionButton();

    expectMatch(isBottomSheetPresent, true);
    expectMatch(isPrescriptionButtonPresent, true);
  }

  @override
  RegExp get pattern => RegExp(r"I have BottomSheet view and options on screen");
}
