import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../../page/signup_health_card_page.dart';

class ClickEnableCamera extends Then1WithWorld<String, FlutterWorld> {
  @override
  Future<void> executeStep(String imageId) async {
    final image = find.bySemanticsLabel(imageId);
    await FlutterDriverUtils.tap(world.driver, image);
  }

  @override
  RegExp get pattern => RegExp(r"I tap on the camera with {string} option");
}
