import 'dart:io';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../../page/signup_health_card_page.dart';

class CheckHealthCardPresent extends GivenWithWorld<FlutterWorld> {
  @override
  Future<void> executeStep() async {
    SignupHealthProcess process = SignupHealthProcess(world.driver);
    bool isHealthPresent = await process.getHealthView();
    bool isUploadButtonPresent = await process.getUploadButton();

    print(isHealthPresent);
    print(isUploadButtonPresent);

    expectMatch(isHealthPresent, true);
    expectMatch(isUploadButtonPresent, true);
  }

  @override
  RegExp get pattern => RegExp(r"I have HealthCard view and other widgets on screen");
}
