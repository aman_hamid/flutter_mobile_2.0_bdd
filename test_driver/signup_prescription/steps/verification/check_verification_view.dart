import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../../page/otp_verify_page.dart';

class CheckVerificationPresent extends GivenWithWorld<FlutterWorld> {
  @override
  Future<void> executeStep() async {
    VerificationProcess process = VerificationProcess(world.driver);
    bool isVerificationViewPresent = await process.getVerificationView();
    bool isPasswordPresent = await process.getPasswordView();
    bool isOtpViewPresent = await process.getOtpView();
    bool isResendRecallPresent = await process.getResendRecallButton();
    bool isNextButtonPresent = await process.getCompleteButton();
    bool isChangeButtonPresent = await process.getChangeButton();

    expectMatch(isVerificationViewPresent, true);
    expectMatch(isPasswordPresent, true);
    expectMatch(isOtpViewPresent, true);
    expectMatch(isResendRecallPresent, true);
    expectMatch(isNextButtonPresent, true);
    expectMatch(isChangeButtonPresent, true);
  }

  @override
  RegExp get pattern => RegExp(r"I have Verification view and all other widgets on screen");
}
