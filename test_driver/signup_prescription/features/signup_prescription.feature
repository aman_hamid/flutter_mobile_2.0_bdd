Feature: Feature for checking the signup flow with transfer

  Scenario: Validates the phone field and Signup page is visible
    Then I pause for 5 seconds
    Given I have phoneField and proceedButton
    When I fill the "phoneField" field with "1234205006"
    Then I tap the "buttonProceed" button
    Then I pause for 3 seconds


  Scenario: Validates the Signup view and widgets
    Given I have Signup view and all other widgets on screen
    Then I fill the "firstName" field with "Mark"
    And I fill the "lastName" field with "42"
    And I fill the "dobMM" field with "11"
    And I fill the "dobDD" field with "09"
    And I fill the "dobYYYY" field with "1992"
    Then I tap the "buttonContinue" button
    And I pause for 5 seconds

  Scenario: Validates the Transfer view and widgets
    Given I have Transfer view and all other widgets on screen
    And I pause for 6 seconds
    Then I check i will do option visible on screen
    And I tap on the will do later option
    Then I pause for 3 seconds


  Scenario: Validates the BottomSheet view and widgets
    Given I have BottomSheet view and options on screen
    And I pause for 2 seconds
    Then I tap on prescription option
    And I pause for 3 seconds

  Scenario: Validates the HealthCard view and widgets
    Given I have HealthCard view and other widgets on screen
    And I pause for 2 seconds
    Then I tap on "image0" position
    Then I tap the "openGallery" button
    And I pause for 3 seconds
    Then I tap the "addNewImage" button
    And I pause for 3 seconds
    Then I tap on "image1" position
    Then I tap the "openGallery" button
    And I pause for 3 seconds
    Then I tap the "uploadButton" button
    And I pause for 3 seconds

  Scenario: Validates the Almost done view and widgets
    Given I have Almost done view and all other widgets on screen
    Then I tap on the edd dropdown button
    And I tap on the "SIX_DAYS" option
    Then I tap on the province dropdown button
    And I tap on the "british_columbia" option
    Then I tap on the gender "MALE" button
    And I fill the "addEmail" field with "test05005@gmail.com"
    Then I tap on the next button from almost done
    And  I pause for 3 seconds

  Scenario: Validates the Verification view and widgets
    Given I have Verification view and all other widgets on screen
    Then I tap on the resend or call button
    And I pause for 3 seconds
    And I tap on the close button
    Then I tap on the phone number change button
    And I pause for 3 seconds
    And  I fill the "verifyPhoneView" field with "1234205007"
    Then I tap on the update button
    And I pause for 3 seconds
    And I fill the "verifyOTP" field with "5689"
    And I fill the "verifyPassword" field with "12345678"
    Then I tap on the continue button
    And I pause for 8 seconds
    Then I have "DashBoard" on screen

