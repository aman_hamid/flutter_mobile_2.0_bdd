import 'dart:io';

import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';
import 'package:glob/glob.dart';

import 'steps/about_you/check_about_you.dart';
import 'steps/almost_done/check_almost_done.dart';
import 'steps/almost_done/click_gender_option.dart';
import 'steps/almost_done/click_province_option.dart';
import 'steps/almost_done/click_provins_drop_down.dart';
import 'steps/almost_done/click_tap_drop_down.dart';
import 'steps/dashboard/check_dashboard.dart';
import 'steps/health_card/click_enable_camera.dart';
import 'steps/health_card/click_tap_on_image_one.dart';
import 'steps/landing/check_landing_widget.dart';
import 'steps/transfer/check_i_will_do_bottomsheet.dart';
import 'steps/transfer/check_transfer.dart';
import 'steps/transfer/check_transfer_skip_visible.dart';
import 'steps/transfer/click_tap_on_prescription_option.dart';
import 'steps/verification/check_verification_view.dart';
import 'steps/almost_done/click_change_pharmacy.dart';
import 'steps/verification/click_change_phone.dart';
import 'steps/verification/click_complete_button.dart';
import 'steps/almost_done/click_edd_drop_down_option.dart';

import 'steps/almost_done/click_next_button.dart';
import '../signup_prescription/steps/landing/click_proceed_landing.dart';

import 'steps/verification/click_recall_and_resend.dart';

import 'steps/verification/click_update_phone.dart';
import 'steps/verification/click_view_dismiss.dart';
import 'steps/health_card/check_signup_health.dart';
import 'steps/transfer/click_tap_i_will_do_later.dart';

Future<void> main() async {
  final config = FlutterTestConfiguration()
    ..features = [Glob(r"test_driver/signup_prescription/features/**.feature")]
    ..buildFlavor = "staging"
    ..stopAfterTestFailed = true
    ..reporters = [
      ProgressReporter(),
      TestRunSummaryReporter(),
      JsonReporter(path: 'test_driver/signup_prescription/signup_prescription_report.json')
    ] // you can include the "StdoutReporter()" without the message level parameter for verbose log information
    ..stepDefinitions = [
      CheckGivenWidgets(),
      ClickProceedButton(),
      CheckSignupViewPresent(),
      CheckTransferViewPresent(),
      CheckSkipOptionPresent(),
      ClickIWillDoLater(),
      CheckBottomSheetPresent(),
      ClickPrescription(),
      CheckHealthCardPresent(),
      ClickImageOne(),
      ClickEnableCamera(),
      CheckAlmostViewPresent(),
      ClickChangePharmacyButton(),
      ClickDropDownButton(),
      ClickEddOptionButton(),
      ClickProvinceDropDownButton(),
      ClickProvinceDropOption(),
      ClickGenderButton(),
      ClickNextButton(),
      CheckVerificationPresent(),
      ClickResendRecall(),
      ClickViewDismiss(),
      ClickChangePhone(),
      ClickUpdate(),
      ClickComplete(),
      CheckDashBoardPresent()
    ]
    ..logFlutterProcessOutput = true // uncomment to see command invoked to start the flutter test app
    ..restartAppBetweenScenarios = false
    ..targetAppPath = "test_driver/signup_prescription/signup_prescription.dart";
  return GherkinRunner().execute(config);
}

Future<void> createPermission() async {
  await Process.run('/Users/sefin/Library/Android/sdk/platform-tools/adb', ['shell', 'pm', 'grant', 'com.pocketpills.staging', 'android.permission.CAMERA']);
}
