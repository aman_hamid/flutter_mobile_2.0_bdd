import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';

class SignupHealthProcess {
  final healthView = find.byValueKey('healthView');
  final uploadButton = find.byValueKey('uploadButton');
  final openCamera = find.byValueKey('openCamera');
  final openGallery = find.byValueKey('openGallery');

  FlutterDriver? _driver;

  //Constructor
  SignupHealthProcess(FlutterDriver? driver) {
    this._driver = driver;
  }

  Future<bool> getHealthView() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, healthView);
    return value;
  }

  Future<bool> getUploadButton() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, uploadButton);
    return value;
  }

  Future<bool> getImageOne() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, uploadButton);
    return value;
  }

  Future<bool> getImageTwo() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, uploadButton);
    return value;
  }

  Future<void> tapOnUploadButton() async {
    await FlutterDriverUtils.tap(_driver!, uploadButton);
  }

  Future<void> tapOnOpenCamera() async {
    await FlutterDriverUtils.tap(_driver!, openCamera);
  }

  Future<void> tapOnOpenGallery() async {
    await FlutterDriverUtils.tap(_driver!, openGallery);
  }
}
