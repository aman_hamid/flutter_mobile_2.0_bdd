import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';

class TransferProcess {
  final transferView = find.byValueKey('TransferView');
  final skipButton = find.byValueKey('SkipText');
  final iWillDoLater = find.byValueKey('IWillDoLater');
  final searchClick = find.byValueKey('searchClick');
  final buttonTransfer = find.byValueKey('buttonTransfer');
  final prescriptionFlow = find.byValueKey('prescriptionFlow');
  final telehealthFlow = find.byValueKey('telehealthFlow');
  final searchMedicine = find.byValueKey('searchMedicine');
  final openVideo = find.byValueKey('openVideo');
  final bottomSheetView = find.byValueKey('bottomSheetView');

  FlutterDriver? _driver;

  //Constructor
  TransferProcess(FlutterDriver? driver) {
    this._driver = driver;
  }

  Future<bool> getTransfer() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, transferView);
    return value;
  }

  Future<bool> getSkipButton() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, skipButton);
    return value;
  }

  Future<bool> getIWillDoLate() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, iWillDoLater);
    return value;
  }

  Future<void> getIWillDoLater() async {
    await FlutterDriverUtils.tap(_driver!, iWillDoLater);
  }

  Future<bool> getBottomSheet() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, bottomSheetView);
    return value;
  }

  Future<bool> getPrescriptionButton() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, bottomSheetView);
    return value;
  }

  Future<void> tapOnPrescriptionButton() async {
    await FlutterDriverUtils.tap(_driver!, prescriptionFlow);
  }

  Future<bool> getButtonTransfer() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, buttonTransfer);
    return value;
  }
}
