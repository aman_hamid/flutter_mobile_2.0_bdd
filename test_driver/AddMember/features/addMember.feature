Feature: To validate Add member flow is working correctly

    Scenario: Validates member 1st member adding screen
        Then I pause for 5 seconds
        When I fill the "phoneField" field with "1111111111"
        Then I tap the "buttonProceed" button
        Then I pause for 3 seconds
        Given I have "LoginPage" on screen
        Then I pause for 3 seconds
        When I fill the "phoneFieldLogin" field with "1111111111"
        When I fill the "loginPasswordField" field with "123456789"
        Then I tap the "loginProceed" button
        Then I pause for 5 seconds
        Given I have "DashBoard" on screen
        Then I pause for 5 seconds
        Then I tap the "addMemberButton" button
        Then I pause for 5 seconds
        When I fill the "addMemberFName" field with "test"
        When I fill the "addMemberLName" field with "test"
        Then I fill the "addMemberDate" field with "02"
        Then I fill the "addMemberMonth" field with "09"
        Then I fill the "addMemberYear" field with "1999"
        Then I tap the "RelationRadioGroup" button
        Then I tap the "GenderRadioGroup" button
        Then I tap the "addMemberSubmit" button
        Then I pause for 3 seconds
        When I fill the "addMemberPhone" field with "1111010102"
        Then I tap the "addMemberPhoneClick" button
        Then I pause for 5 seconds
        Then I tap the "provinceDropDown" button
        Then I tap the "british_columbia" button
        Then I tap the "MedicineRadioGroup" button
        Then I tap the "addMemberSubmit" button

