import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';

class VerificationProcess {
  final verificationView = find.byValueKey('VerificationView');
  final verifyCompleteButton = find.byValueKey('verifyCompleteButton');
  final changeButton = find.byValueKey('changeButton');
  final verifyOTP = find.byValueKey('verifyOTP');
  final resendOrRecall = find.byValueKey('resendOrRecall');
  final verifyPassword = find.byValueKey('verifyPassword');
  final verifyPhoneView = find.byValueKey('verifyPhoneView');
  final phoneChangeDialogue = find.byValueKey('phoneChangeDialogue');
  final buttonUpdate = find.byValueKey('buttonUpdate');
  final closeBottomSheet = find.byValueKey('closeBottomSheet');

  FlutterDriver? _driver;

  //Constructor
  VerificationProcess(FlutterDriver? driver) {
    this._driver = driver;
  }

  Future<bool> getVerificationView() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, verificationView);
    return value;
  }

  Future<void> getViewDismiss() async {
    await FlutterDriverUtils.tap(_driver!, closeBottomSheet);
  }

  Future<bool> getOtpView() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, verifyOTP);
    return value;
  }

  Future<bool> getPasswordView() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, verifyPassword);
    return value;
  }

  Future<bool> getPhoneView() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, verifyPhoneView);
    return value;
  }

  Future<bool> getDialogueView() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, phoneChangeDialogue);
    return value;
  }

  Future<bool> getCompleteButton() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, verifyCompleteButton);
    return value;
  }

  Future<void> tapOnCompleteButton() async {
    await FlutterDriverUtils.tap(_driver!, verifyCompleteButton);
  }

  Future<void> tapOnUpdateButton() async {
    await FlutterDriverUtils.tap(_driver!, buttonUpdate);
  }

  Future<bool> getChangeButton() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, changeButton);
    return value;
  }

  Future<void> tapOnChangeButton() async {
    await FlutterDriverUtils.tap(_driver!, changeButton);
  }

  Future<bool> getResendRecallButton() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, resendOrRecall);
    return value;
  }

  Future<void> tapOnResendRecallButton() async {
    await FlutterDriverUtils.tap(_driver!, resendOrRecall);
  }
}
