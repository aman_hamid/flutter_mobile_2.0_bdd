import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../../page/transfer_page.dart';

class ClickIWillDoLater extends AndWithWorld<FlutterWorld> {
  @override
  Future<void> executeStep() async {
    TransferProcess process = TransferProcess(world.driver);
    await process.getIWillDoLater();
  }

  @override
  RegExp get pattern => RegExp(r"I tap on the will do later option");
}
