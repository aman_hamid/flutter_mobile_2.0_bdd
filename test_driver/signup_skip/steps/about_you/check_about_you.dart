import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../../page/signup_page.dart';

class CheckSignupViewPresent extends GivenWithWorld<FlutterWorld> {
  @override
  Future<void> executeStep() async {
    SignupProcess process = SignupProcess(world.driver);
    bool isSignupPresent = await process.getSignupView();
    bool isFirstNamePresent = await process.getFirstName();
    bool isLastNamePresent = await process.getLastName();
    bool isDobMMPresent = await process.getDobMM();
    bool isDobDDPresent = await process.getDobDD();
    bool isDobYYYYPresent = await process.getDobYYYY();
    bool isBtnContinuePresent = await process.getContinueButton();
    expectMatch(isSignupPresent, true);
    expectMatch(isFirstNamePresent, true);
    expectMatch(isLastNamePresent, true);
    expectMatch(isDobMMPresent, true);
    expectMatch(isDobDDPresent, true);
    expectMatch(isDobYYYYPresent, true);
    expectMatch(isBtnContinuePresent, true);
  }

  @override
  RegExp get pattern => RegExp(r"I have Signup view and all other widgets on screen");
}
