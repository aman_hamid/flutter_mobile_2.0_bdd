import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../../page/almost_done_page.dart';

class CheckAlmostViewPresent extends GivenWithWorld<FlutterWorld> {
  @override
  Future<void> executeStep() async {
    AlmostDoneProcess process = AlmostDoneProcess(world.driver);
    bool isAlmostPresent = await process.getAlmostView();
    bool isAddEmailPresent = await process.getAddEmail();
    bool isGenderRadioGroupPresent = await process.getGenderRadio();
    bool isPharmacyVisiblePresent = await process.getGenderRadio();
    bool isChangePharmacyPresent = await process.getGenderRadio();
    bool isProvinceDropDownPresent = await process.getProvinceDropDown();
    bool isEddDropDownPresent = await process.getEddDropDown();
    print(isAlmostPresent);
    print(isAddEmailPresent);
    print(isGenderRadioGroupPresent);
    print(isPharmacyVisiblePresent);
    print(isChangePharmacyPresent);
    print(isEddDropDownPresent);
    print(isProvinceDropDownPresent);

    expectMatch(isAlmostPresent, true);
    //expectMatch(isProvinceDropDownPresent, true);
    expectMatch(isAddEmailPresent, true);
    expectMatch(isGenderRadioGroupPresent, true);
    expectMatch(isPharmacyVisiblePresent, true);
    expectMatch(isChangePharmacyPresent, true);
  }

  @override
  RegExp get pattern => RegExp(r"I have Almost done view and all other widgets on screen");
}
