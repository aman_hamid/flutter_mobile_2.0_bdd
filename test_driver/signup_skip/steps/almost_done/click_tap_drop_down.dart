import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../../page/almost_done_page.dart';

class ClickDropDownButton extends ThenWithWorld<FlutterWorld> {
  @override
  Future<void> executeStep() async {
    AlmostDoneProcess process = AlmostDoneProcess(world.driver);
    await process.tapOnDrpDown();
  }

  @override
  RegExp get pattern => RegExp(r"I tap on the edd dropdown button");
}
