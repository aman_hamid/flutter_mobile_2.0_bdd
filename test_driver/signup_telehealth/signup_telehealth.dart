/// Behavioral Driven Development

import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/services.dart';
import 'package:flutter_driver/driver_extension.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pocketpills/main_staging.dart' as app;
import 'package:pocketpills/utils/localization/localization_utils.dart';

void main() async {
  // This line enables the extension.
  enableFlutterDriverExtension();
  // await Process.run('/Users/sefin/Library/Android/sdk/platform-tools/adb', ['shell', 'pm', 'grant', 'com.pocketpills.staging', 'android.permission.CAMERA']);
  // Call the `main()` function of the app, or call `runApp` with

  // const MethodChannel channel = MethodChannel('flutter.baseflow.com/permissions/methods');
  //
  // channel.setMockMethodCallHandler((MethodCall handler2) async {
  //   return <int, int>{
  //     Permission.camera.value: 1,
  //     Permission.storage.value: 1,
  //     Permission.photos.value: 1,
  //     Permission.contacts.value: 1,
  //   }; // set initial values here if desired
  // });
  isIntegration = true;
  const MethodChannel channel2 = MethodChannel('plugins.flutter.io/image_picker');
  channel2.setMockMethodCallHandler((MethodCall handler) async {
    ByteData data = await rootBundle.load('graphics/ppLogo.png');
    Uint8List bytes = data.buffer.asUint8List();
    Directory tempDir = await getTemporaryDirectory();
    File file = await File(
      '${tempDir.path}/tmp.png',
    ).writeAsBytes(bytes);
    print(file.path);
    return file.path;
  });

  app.main();
}
