import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../../page/transfer_page.dart';

class ClickTelehealth extends ThenWithWorld<FlutterWorld> {
  @override
  Future<void> executeStep() async {
    TransferProcess process = TransferProcess(world.driver);
    await process.tapOnTelehealthButton();
  }

  @override
  RegExp get pattern => RegExp(r"I tap on telehealth option");
}
