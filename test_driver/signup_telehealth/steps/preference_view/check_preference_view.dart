import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../../page/preference_page.dart';

class CheckPreferencePresent extends GivenWithWorld<FlutterWorld> {
  @override
  Future<void> executeStep() async {
    PreferenceProcess process = PreferenceProcess(world.driver);
    bool isHealthPresent = await process.getPreferenceView();
    bool isUploadButtonPresent = await process.getSubmitButton();

    print(isHealthPresent);
    print(isUploadButtonPresent);

    expectMatch(isHealthPresent, true);
    expectMatch(isUploadButtonPresent, true);
  }

  @override
  RegExp get pattern => RegExp(r"I have Preference view on screen");
}
