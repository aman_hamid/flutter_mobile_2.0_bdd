import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

class ClickGenderButton extends Then1WithWorld<String, FlutterWorld> {
  @override
  Future<void> executeStep(String loginbtn) async {
    var idString;
    if (loginbtn == "MALE") {
      idString = find.byValueKey("gender0");
    } else if (loginbtn == "FEMALE") {
      idString = find.byValueKey("gender1");
    } else {
      idString = find.byValueKey("gender2");
    }
    await FlutterDriverUtils.tap(world.driver, idString);
  }

  @override
  RegExp get pattern => RegExp(r"I tap on the gender {string} button");
}
