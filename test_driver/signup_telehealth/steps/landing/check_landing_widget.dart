import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../../page/signup_page.dart';

class CheckGivenWidgets extends GivenWithWorld<FlutterWorld> {
  CheckGivenWidgets() : super(StepDefinitionConfiguration()..timeout = Duration(seconds: 10));

  @override
  Future<void> executeStep() async {
    SignupProcess landingPage = SignupProcess(world.driver);
    bool textWidget = await landingPage.getTextField();
    bool buttonWidget = await landingPage.getButtonProceed();
    expectMatch(textWidget, true);
    expectMatch(buttonWidget, true);
  }

  @override
  RegExp get pattern => RegExp(r"I have phoneField and proceedButton");
}
