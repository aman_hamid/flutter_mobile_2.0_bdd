import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../../page/date_selection_page.dart';

class CheckDateSelectionPresent extends GivenWithWorld<FlutterWorld> {
  @override
  Future<void> executeStep() async {
    DateSelectionProcess process = DateSelectionProcess(world.driver);
    bool isHealthPresent = await process.getDateView();
    bool isUploadButtonPresent = await process.getSubmitButton();

    print(isHealthPresent);
    print(isUploadButtonPresent);

    expectMatch(isHealthPresent, true);
    expectMatch(isUploadButtonPresent, true);
  }

  @override
  RegExp get pattern => RegExp(r"I have Date selection view on screen");
}
