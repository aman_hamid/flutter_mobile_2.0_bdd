import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../../page/almost_done_page.dart';
import '../../page/country_select_page.dart';

class CheckCountryListPresent extends GivenWithWorld<FlutterWorld> {
  @override
  Future<void> executeStep() async {
    CountrySelectionProcess process = CountrySelectionProcess(world.driver);
    bool isCountyViewPresent = await process.getCountryView();
    bool isContinueButtonPresent = await process.getContinueButton();

    expectMatch(isCountyViewPresent, true);
    expectMatch(isContinueButtonPresent, true);
  }

  @override
  RegExp get pattern => RegExp(r"I have country list view on screen");
}
