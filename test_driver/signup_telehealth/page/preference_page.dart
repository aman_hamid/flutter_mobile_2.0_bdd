import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';

class PreferenceProcess {
  final telehealthPreferenceView = find.byValueKey('telehealthPreferenceView');
  final continueButton = find.byValueKey('continueButton');

  FlutterDriver? _driver;

  //Constructor
  PreferenceProcess(FlutterDriver? driver) {
    this._driver = driver;
  }

  Future<bool> getPreferenceView() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, telehealthPreferenceView);
    return value;
  }

  Future<bool> getSubmitButton() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, continueButton);
    return value;
  }
}
