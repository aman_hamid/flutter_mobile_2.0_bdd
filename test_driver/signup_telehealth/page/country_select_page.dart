import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';

class CountrySelectionProcess {
  final countryView = find.byValueKey('countryListView');
  final continueButton = find.byValueKey('continueButton');

  FlutterDriver? _driver;

  //Constructor
  CountrySelectionProcess(FlutterDriver? driver) {
    this._driver = driver;
  }

  Future<bool> getCountryView() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, countryView);
    return value;
  }

  Future<bool> getContinueButton() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, continueButton);
    return value;
  }
}
