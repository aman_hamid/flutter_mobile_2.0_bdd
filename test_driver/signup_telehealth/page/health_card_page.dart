import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';

class HealthProcess {
  final healthView = find.byValueKey('healthSignupView');
  final uploadButton = find.byValueKey('uploadSignupButton');
  final openCamera = find.byValueKey('openCameraSignup');
  final openGallery = find.byValueKey('openGallerySignup');
  final imageOneSignup = find.byValueKey('imageOneSignup');
  final imageTwoSignup = find.byValueKey('imageTwoSignup');

  FlutterDriver? _driver;

  //Constructor
  HealthProcess(FlutterDriver? driver) {
    this._driver = driver;
  }

  Future<bool> getHealthView() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, healthView);
    return value;
  }

  Future<bool> getUploadButton() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, uploadButton);
    return value;
  }

  Future<bool> getImageOne() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, uploadButton);
    return value;
  }

  Future<bool> getImageTwo() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, uploadButton);
    return value;
  }

  Future<void> tapOnUploadButton() async {
    await FlutterDriverUtils.tap(_driver!, uploadButton);
  }

  Future<void> tapOnOpenCamera() async {
    await FlutterDriverUtils.tap(_driver!, openCamera);
  }

  Future<void> tapOnOpenGallery() async {
    await FlutterDriverUtils.tap(_driver!, openGallery);
  }
}
