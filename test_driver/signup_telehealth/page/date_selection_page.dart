import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';

class DateSelectionProcess {
  final dateSelectionView = find.byValueKey('dateSelectionView');
  final dateSubmit = find.byValueKey('dateSubmit');
  final doctorsViewPopup = find.byValueKey('doctorsViewPopup');

  FlutterDriver? _driver;

  //Constructor
  DateSelectionProcess(FlutterDriver? driver) {
    this._driver = driver;
  }

  Future<bool> getDateView() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, dateSelectionView);
    return value;
  }

  Future<bool> getSubmitButton() async {
    var value = await FlutterDriverUtils.isPresent(_driver!, dateSubmit);
    return value;
  }
}
