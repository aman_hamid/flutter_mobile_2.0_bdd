import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

class CheckWidgetPresent extends Given1WithWorld<String, FlutterWorld> {
  @override
  Future<void> executeStep(String input1) async {
    final homefinder = find.byValueKey(input1);
    bool isPresent = await FlutterDriverUtils.isPresent(world.driver, homefinder);
    expect(isPresent, true);
  }

  @override
  RegExp get pattern => RegExp(r"I have {string} on screen");
}
