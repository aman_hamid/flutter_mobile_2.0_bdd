Feature: Main Screen Validates and then Logins are ok

    Scenario: Validates the phone field and login page is visible
        Then I pause for 9 seconds
        Then I pause for 9 seconds
        When I fill the "phoneField" field with "1111111111"
        Then I tap the "buttonProceed" button
        Then I pause for 3 seconds
        Given I have "LoginPage" on screen
        Then I pause for 3 seconds
        When I fill the "phoneFieldLogin" field with "1111111111"
        When I fill the "loginPasswordField" field with "123456789"
        Then I tap the "loginProceed" button
        Then I pause for 5 seconds
        Given I have "DashBoard" on screen
        Then I pause for 3 seconds
#        Then I tap the "menuItemTap" button
#        Then I tap the "logoutTap" button
#        Then I pause for 2 seconds
