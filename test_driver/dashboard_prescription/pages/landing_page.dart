import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';

class LandingPage {
  final txtValue = find.byValueKey('phoneField');
  final btnProceed = find.byValueKey('buttonProceed');

  FlutterDriver? _driver;

  //Constructor
  LandingPage(FlutterDriver? driver) {
    this._driver = driver;
  }

  Future<bool> getTextFieldProceed() async {
  return await FlutterDriverUtils.isPresent(_driver, txtValue);
  }

  Future<bool> getButtonProceed() async {
    return await FlutterDriverUtils.isPresent(_driver, btnProceed);
  }

  getCounterValue() async {
   await FlutterDriverUtils.isPresent(_driver, txtValue);
  }

  Future<void> clickBtnPlus() async {
    return await _driver!.tap(btnProceed);
  }

  Future<String> getCounterValueSecond() async {
    return await _driver!.getText(txtValue);
  }

  Future<void> clickBtnPlusSecond() async {
    return await _driver!.tap(btnProceed);
  }
}
