Feature: It is for adding prescription from dashboard

    Scenario: Complete telehealth booking through dash board

        Then I pause for 5 seconds
        When I fill the "phoneField" field with "9090909090"
        Then I tap the "buttonProceed" button
        Then I pause for 3 seconds
        Given I have "LoginPage" on screen
        Then I pause for 3 seconds
        When I fill the "phoneFieldLogin" field with "9090909090"
        When I fill the "loginPasswordField" field with "00000000"
        Then I tap the "loginProceed" button
        Then I pause for 5 seconds
        Given I have "DashBoard" on screen
        Then I pause for 5 seconds
        Given Then I tap the "square UPLOAD_PRESCRIPTION" button
        And I pause for 2 seconds
        Then I tap the "image0" button
        Then I tap the "openGallery" button
        And I pause for 3 seconds
        Then I tap the "addNewImage" button
        And I pause for 3 seconds
        Then I tap the "image1" button
        Then I tap the "openGallery" button
        And I pause for 3 seconds
        Then I tap the "uploadButton" button
        And I pause for 3 seconds
