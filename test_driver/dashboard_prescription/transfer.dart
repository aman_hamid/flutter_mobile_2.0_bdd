/// Behavioral Driven Development

import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/services.dart';
import 'package:flutter_driver/driver_extension.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pocketpills/main_staging.dart' as app;
import 'package:pocketpills/utils/localization/localization_utils.dart';

void main() {
  // This line enables the extension.
  enableFlutterDriverExtension();

  // Call the `main()` function of the app, or call `runApp` with
  // any widget you are interested in testing.
  isIntegration = true;
  const MethodChannel channel2 = MethodChannel('plugins.flutter.io/image_picker');
  channel2.setMockMethodCallHandler((MethodCall handler) async {
    ByteData data = await rootBundle.load('graphics/ppLogo.png');
    Uint8List bytes = data.buffer.asUint8List();
    Directory tempDir = await getTemporaryDirectory();
    File file = await File(
      '${tempDir.path}/tmp.png',
    ).writeAsBytes(bytes);
    print(file.path);
    return file.path;
  });
  app.main();
}
