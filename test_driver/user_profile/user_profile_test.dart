import 'dart:async';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';
import 'package:glob/glob.dart';

import 'steps/check_widget_present.dart';
import 'steps/select_on_widget_value.dart';
import 'steps/tap_on_button.dart';
import 'steps/tap_on_dropdown.dart';
import 'steps/tap_on_tabs.dart';

Future<void> main() {
  final config = FlutterTestConfiguration()
    ..features = [Glob(r"test_driver/user_profile/features/**.feature")]
    ..buildFlavor = "staging"
    ..stopAfterTestFailed = true
    ..reporters = [
      ProgressReporter(),
      TestRunSummaryReporter(),
      JsonReporter(path: 'test_driver/user_profile/user_profile_report.json')
    ] // you can include the "StdoutReporter()" without the message level parameter for verbose log information
    ..stepDefinitions = [CheckWidgetPresent(), ClickButton(), SelectWidgetValue(), TapOnDropDown(), TapOnTab()]
    ..logFlutterProcessOutput = true // uncomment to see command invoked to start the flutter test app
    ..restartAppBetweenScenarios = false
    ..targetAppPath = "test_driver/user_profile/user_profile.dart";
  return GherkinRunner().execute(config);
}
