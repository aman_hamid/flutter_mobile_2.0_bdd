Feature: Main Screen Validates and then Logins are ok

  Scenario: Validates the phone field and login page is visible
    Then I pause for 5 seconds
    When I fill the "phoneField" field with "1190119011"
    Then I tap the "buttonProceed" button
    Then I pause for 3 seconds
    Given I have "LoginPage" on screen
    Then I pause for 3 seconds
    When I fill the "phoneFieldLogin" field with "1190119011"
    When I fill the "loginPasswordField" field with "12345678"
    Then I tap the "loginProceed" button
    Then I pause for 5 seconds
    Given I have "DashBoard" on screen
    Then I pause for 8 seconds
    Then I tap the "menuItemTap" button
    Then I tap the "profileTap" button
    Then I pause for 3 seconds

  Scenario: Validates the Profile view and widgets
    And I pause for 5 seconds
    Then I tap the "languageDropDown" dropdown
    Then I select "fr" option
    And I pause for 3 seconds
    Then I tap the "updateButton" button
    Then I pause for 3 seconds
    Then I tap the "languageDropDown" dropdown
    Then I select "en" option
    And I pause for 2 seconds
    Then I tap the "callDayDropDown" dropdown
    Then I select "WEEKENDS" option
    And I pause for 2 seconds
    Then I tap the "callTimeDropDown" dropdown
    Then I select "8 AM - 12 NOON" option
    Then I tap the "updateButton" button
    And I pause for 5 seconds
    Then I tap the "user_health_card" tab

  Scenario: Validates the Profile Health view and widgets
    And I pause for 5 seconds
    Then I tap the "user_health_card" tab
    And I pause for 5 seconds
    Then I tap the "edit_image_1" button
    Then I tap the "open gallery" button
    And I pause for 5 seconds

  Scenario: Validates the Profile Insurance view and widgets
    And I pause for 5 seconds
    Then I tap the "user_insurance" tab
    And I pause for 2 seconds
    Then I tap the "add insurance" button
    Then I tap the "secondaryFront" button
    Then I tap the "image_secondaryInsuranceFrontImage" button
    And I pause for 2 seconds
    Then I tap the "secondaryBack" button
    Then I tap the "image_secondaryInsuranceBackImage" button
    And I pause for 2 seconds
    Then I tap the "delete_image_secondaryInsuranceFrontImage" button
    Then I tap the "delete_image_secondaryInsuranceBackImage" button
    And I pause for 3 seconds

  Scenario: Validates the Profile address view and widgets
    Then I tap the "user_address" tab
    And I pause for 3 seconds
    Then I tap the "addNewAddress" button
    Given I have "Address add or edit view" on screen
    When I fill the "addressName" field with "New Home"
    And I pause for 9 seconds
    Then I tap the "saveAddress" button
    And I pause for 8 seconds
    Then I tap the "editAddress0" button
    Then I tap the "saveAddress" button
    And I pause for 8 seconds
    Then I tap the "deleteAddress0" button
    And I pause for 2 seconds
    Then I tap the "cancelAddress" button
    And I pause for 1 seconds
    Then I tap the "deleteAddress0" button
    And I pause for 2 seconds
    Then I tap the "deleteAddress" button
    And I pause for 2 seconds

  Scenario: Validates the Profile payment view and widgets
    Then I tap the "user_payment" tab
    And I pause for 3 seconds
    Then I tap the "add new card" button
    And I pause for 9 seconds
    Then I tap the "goBack" button
    And I pause for 3 seconds

  Scenario: Validates the Profile health view and widgets
    Then I tap the "user_health" tab
    And I pause for 3 seconds
    Then I tap the "YES" button
    And I pause for 3 seconds
    Then I tap the "NO" button
    And I pause for 3 seconds
    Then I tap the "YES" button
    And I pause for 3 seconds
    Then I fill the "allergies" field with "Asthma,Dermatitis"
    And I fill the "meds and vitamins" field with "Pyridoxine,Cobalamin"
    Then I tap the "save" button
    And I pause for 3 seconds



