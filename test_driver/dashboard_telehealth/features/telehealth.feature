Feature: Main Screen Validates and then Logins are ok

#    Scenario: Validates the phone field and login page is visible
#        Then I pause for 5 seconds
#        When I fill the "phoneField" field with "1111111111"
#        Then I tap the "proceedButton" button
#        Then I pause for 3 seconds
#        Given I have "LoginPage" on screen
#        Then I pause for 3 seconds
#        When I fill the "phoneFieldLogin" field with "1111111111"
#        When I fill the "loginPasswordField" field with "123456789"
#        Then I tap the "loginProceed" button
#        Then I pause for 5 seconds
#        Given I have "DashBoard" on screen
#        Then I pause for 3 seconds
#        Then I tap the "menuItemTap" button
#        Then I tap the "logoutTap" button
#        Then I pause for 2 seconds
#    UPLOAD_PRESCRIPTION
#    MEDICATION_SEARCH
#    TRANSFER
#    TELEHEALTH

    Scenario: Complete telehealth booking through dash board

        Then I pause for 5 seconds
        When I fill the "phoneField" field with "9090909090"
        Then I tap the "buttonProceed" button
        Then I pause for 3 seconds
        Given I have "LoginPage" on screen
        Then I pause for 3 seconds
        When I fill the "phoneFieldLogin" field with "9090909090"
        When I fill the "loginPasswordField" field with "00000000"
        Then I tap the "loginProceed" button
        Then I pause for 5 seconds
        Given I have "DashBoard" on screen
        Then I pause for 5 seconds
        Given Then I tap the "square TELEHEALTH" button
        Then I pause for 3 seconds
        Given Then I tap the "appointment1" button
        Then I pause for 3 seconds
        Given I have "doctorsViewPopup" on screen
        Then I pause for 3 seconds
        Given Then I tap the "dateSubmit" button
        Given I have "preferencePage" on screen
        Given I have Preference view on screen
        And I pause for 2 seconds
        Then I tap the "continueButton" button
        And I pause for 5 seconds