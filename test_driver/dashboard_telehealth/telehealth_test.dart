import 'dart:async';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';
import 'package:glob/glob.dart';

import 'steps/checkWidgetPresent.dart';
import 'steps/clickButton.dart';


Future<void> main() {
  final config = FlutterTestConfiguration()
    ..features = [Glob(r"test_driver/dashboard_telehealth/features/**.feature")]
    ..buildFlavor = "staging"
    ..reporters = [
      ProgressReporter(),
      TestRunSummaryReporter(),
      JsonReporter(path: './report.json')
    ] // you can include the "StdoutReporter()" without the message level parameter for verbose log information
    ..stepDefinitions = [
      CheckWidgetPresent(),
      ClickButton()
    ]
    ..logFlutterProcessOutput = true // uncomment to see command invoked to start the flutter test app
    ..restartAppBetweenScenarios = true
    ..targetAppPath = "test_driver/dashboard_telehealth/transfer.dart";
  return GherkinRunner().execute(config);
}
