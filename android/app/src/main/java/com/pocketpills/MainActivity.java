package com.pocketpills;

import android.os.Bundle;

import com.facebook.appevents.AppEventsLogger;
import com.facebook.appevents.AppEventsConstants;

import java.util.Map;
import androidx.annotation.NonNull;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.*;

public class MainActivity extends FlutterActivity {
    private static final String CHANNEL = "plugins.flutter.io/facebook_analytics";
    AppEventsLogger logger;

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);
        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL)
                .setMethodCallHandler(
                        (call, result) -> {
                            switch (call.method) {
                                case "initialize":
                                    logger = AppEventsLogger.newLogger(getApplicationContext());
                                    result.success("Logger success..");
                                    break;
                                case "logEvent":
                                    handleLogEvent(call, result);
                                    break;
                                case "userData":
                                    handleUserData(call, result);
                                    break;
                                case "identifier":
                                    handleUserIdentifier(call, result);
                                    break;
                                case "initiatedEvent":
                                    logInitiateCheckoutEvent(call, result);
                                    break;
                                case "completeRegistration":
                                    handleCompleteRegistrationEvent(result);
                                    break;
                            }
                        }
                );
    }
    private void handleUserData(MethodCall call, MethodChannel.Result result) {
        final Map<String, String> map = call.argument("userData");
        // AppEventsLogger.setUserData(map.get("email"), map.get("firstName"), map.get("lastName"), map.get("phone"), map.get("dateOfBirth"), map.get("gender"), map.get("city"), map.get("state"), map.get("zip"), map.get("country"));
        result.success("Logger success..");
    }

    private void handleUserIdentifier(MethodCall call, MethodChannel.Result result) {
        final Map<String, String> map = call.argument("identifier");
        //AppEventsLogger.setUserID(map.get("userid"));
        result.success("Logger success..");
    }

    private void handleLogEvent(MethodCall call, MethodChannel.Result result) {
        final String eventName = call.argument("name");
        final Map<String, Object> map = call.argument("parameters");
        final Bundle parameterBundle = createBundleFromMap(map);

        logger.logEvent(eventName, parameterBundle);

        result.success("Logger success..");
    }

    private void handleCompleteRegistrationEvent(MethodChannel.Result result) {
        logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION);
        result.success("Logger success..");
    }

    public void logInitiateCheckoutEvent(MethodCall call, MethodChannel.Result result) {
        final Map<String, Object> map = call.argument("initiatedEvent");
        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT, map.get("contentData").toString());
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, map.get("contentId").toString());
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, map.get("contentType").toString());
        logger.logEvent(AppEventsConstants.EVENT_NAME_INITIATED_CHECKOUT, params);
    }

    private static Bundle createBundleFromMap(Map<String, Object> map) {
        if (map == null) {
            return null;
        }

        Bundle bundle = new Bundle();
        for (Map.Entry<String, Object> jsonParam : map.entrySet()) {
            final Object value = jsonParam.getValue();
            final String key = jsonParam.getKey();
            if (value instanceof String) {
                bundle.putString(key, (String) value);
            } else if (value instanceof Integer) {
                bundle.putInt(key, (Integer) value);
            } else if (value instanceof Long) {
                bundle.putLong(key, (Long) value);
            } else if (value instanceof Double) {
                bundle.putDouble(key, (Double) value);
            } else if (value instanceof Boolean) {
                bundle.putBoolean(key, (Boolean) value);
            } else {
                throw new IllegalArgumentException(
                        "Unsupported value type: " + value.getClass().getCanonicalName());
            }
        }
        return bundle;
    }

}
